#pragma once

#include "Detectors/RASW.h"



class RASW_extended_VJInvoker : public ParallelLoopBody
{
public:
	RASW_extended_VJInvoker( RASW_extended_VJ& _cc, Size _sz1, cv::Point _stripSize, int _xStep, int _yStep, double _factor,
        vector<Rect>& _vec, cv::Point _xThresholds, cv::Point _yThresholds, const Mat& _mask, Mutex* _mtx)
    {
        classifier = &_cc;
        processingRectSize = _sz1;
        stripSize = _stripSize;
        yStep = _yStep;
		xStep = _xStep;
        scalingFactor = _factor;
        rectangles = &_vec;
        mask = _mask;
        mtx = _mtx;
		// parameters for RASW
		deltaXMin=xStep;
		deltaXNom=2;
		deltaXMax=4;
		deltaYMin=yStep;
		deltaYNom=2;
		deltaYMax=4;
		deltaX_t1=_xThresholds.x;
		deltaX_t2=_xThresholds.y;
		deltaY_t1=_yThresholds.x;
		deltaY_t2=_yThresholds.y;
    }

    void operator()(const Range& range) const
    {
        Ptr<FeatureEvaluator> evaluator = classifier->featureEvaluator->clone();

        Size winSize(cvRound(classifier->data.origWinSize.width * scalingFactor), cvRound(classifier->data.origWinSize.height * scalingFactor));

        int y1 = range.start * stripSize.y;
        int y2 = min(range.end * stripSize.y, processingRectSize.height);
		// RASW variables
		int deltaX=1;
		std::vector<int> deltaY=std::vector<int>( processingRectSize.width, yStep);
		int result=0;
        for( int y = y1; y < y2; y += yStep )
        {
            for( int x = 0; x < processingRectSize.width; x += deltaX )
            {
                if ( (!mask.empty()) && (mask.at<uchar>(Point(x,y))==0)) {
                    continue;
                }

				deltaX-=1;
				deltaY[x]-=1;

				// RASW
				if(deltaX==0 && deltaY[x]==0)
				{
					double gypWeight;
					result = classifier->runAt(evaluator, Point(x, y), gypWeight);
					if( result == 1 )
						result =  -(int)classifier->data.stages.size();
					else 
						result = -result;
					if( classifier->data.stages.size() + result < 4 )
					{
						mtx->lock();
						rectangles->push_back(Rect(cvRound(x*scalingFactor), cvRound(y*scalingFactor), winSize.width, winSize.height));
						//rejectLevels->push_back(-result);
						//levelWeights->push_back(gypWeight);
						mtx->unlock();
					}
					//if(result>=15)
						//std::cout << "exit stage " << result << std::endl;

					// modify the x and y steps accordingly
					if(result<deltaX_t1)
						deltaX=deltaXMax;
					else if(result<deltaX_t2)
						deltaX=deltaXNom;
					else
						deltaX=deltaXMin;
					if(result<deltaY_t1)
						deltaY[x]=deltaYMax;
					else if(result<deltaY_t2)
						deltaY[x]=deltaYNom;
					else
						deltaY[x]=deltaYMin;

					//std::cout << "Deltas are : " << deltaX << " t1 " << deltaX_t1 << " t2 " << deltaX_t2 <<  "(" << result << ")" << std::endl;
					//std::cout << "Running VJ at " << cv::Point(x,y) << ". ";
					//std::cout << "Exit stage is : " << -result << "\n";
				}
				else if(deltaX==0)		// close to detection in X direction
					deltaX=deltaXMin;
				else if(deltaY[x]==0)		// close to detection in Y direction
					deltaY[x]=deltaYMin;
				/*if(deltaX<0)
					std::cout << "warning neg deltaX\n";
				if(deltaY[x]<0)
					std::cout << "warning neg deltaY\n";*/
            }
        }
    }

	RASW_extended_VJ* classifier;
    vector<Rect>* rectangles;
    Size processingRectSize;
    int yStep, xStep;
	cv::Point stripSize;
    double scalingFactor;
    vector<int> *rejectLevels;
    vector<double> *levelWeights;
    Mat mask;
    Mutex* mtx;

	//!HERE
	// delta values for x and y directions in RASW
	int deltaXMax;
	int deltaXMin;
	int deltaXNom;
	int deltaYMax;
	int deltaYMin;
	int deltaYNom;
	// exit level thresholds to use with RASW
	int deltaX_t1;
	int deltaX_t2;
	int deltaY_t1;
	int deltaY_t2;
};

RASW_extended_VJ::RASW_extended_VJ()
{
	
}
RASW_extended_VJ::RASW_extended_VJ(const std::string & filename)
{
	load(filename);
}

bool RASW_extended_VJ::empty() const
{
    return oldCascade.empty() && data.stages.empty();
}

bool RASW_extended_VJ::load(const std::string & filename)
{
	
	oldCascade.release();
    data = Data();
    featureEvaluator.release();

    FileStorage fs(filename, FileStorage::READ);
    if( !fs.isOpened() )
        return false;

    if( read(fs.getFirstTopLevelNode()) )
        return true;

    fs.release();

    oldCascade = Ptr<CvHaarClassifierCascade>((CvHaarClassifierCascade*)cvLoad(filename.c_str(), 0, 0, 0));
    return !oldCascade.empty();
}

int RASW_extended_VJ::runAt( Ptr<FeatureEvaluator>& evaluator, Point pt, double& weight )
{
    CV_Assert( oldCascade.empty() );

    assert( data.featureType == FeatureEvaluator::HAAR ||
            data.featureType == FeatureEvaluator::LBP ||
            data.featureType == FeatureEvaluator::HOG );

    if( !evaluator->setWindow(pt) )
        return -1;
    if( data.isStumpBased )
    {
        if( data.featureType == FeatureEvaluator::HAAR )
            return predictOrderedStump<HaarEvaluator>( *this, evaluator, weight );
        else if( data.featureType == FeatureEvaluator::LBP )
            return predictCategoricalStump<LBPEvaluator>( *this, evaluator, weight );
        else if( data.featureType == FeatureEvaluator::HOG )
            return predictOrderedStump<HOGEvaluator>( *this, evaluator, weight );
        else
            return -2;
    }
    else
    {
        if( data.featureType == FeatureEvaluator::HAAR )
            return predictOrdered<HaarEvaluator>( *this, evaluator, weight );
        else if( data.featureType == FeatureEvaluator::LBP )
            return predictCategorical<LBPEvaluator>( *this, evaluator, weight );
        else if( data.featureType == FeatureEvaluator::HOG )
            return predictOrdered<HOGEvaluator>( *this, evaluator, weight );
        else
            return -2;
    }
}

bool RASW_extended_VJ::setImage( Ptr<FeatureEvaluator>& evaluator, const Mat& image )
{
    return empty() ? false : evaluator->setImage(image, data.origWinSize);
}

void RASW_extended_VJ::setMaskGenerator(Ptr<MaskGenerator> _maskGenerator)
{
    maskGenerator=_maskGenerator;
}
Ptr<RASW_extended_VJ::MaskGenerator> RASW_extended_VJ::getMaskGenerator()
{
    return maskGenerator;
}

void RASW_extended_VJ::setFaceDetectionMaskGenerator()
{
/*#ifdef HAVE_TEGRA_OPTIMIZATION
    setMaskGenerator(tegra::getCascadeClassifierMaskGenerator(*this));
#else*/
    setMaskGenerator(Ptr<RASW_extended_VJ::MaskGenerator>());
/*#endif*/
}

struct getRect { Rect operator ()(const CvAvgComp& e) const { return e.rect; } };

// modified to include xStep
void RASW_extended_VJ::detectMultiScale( const Mat& image, vector<Rect>& objects,
                                          double scaleFactor, int minNeighbors,
                                          int flags, Size minObjectSize, Size maxObjectSize, 
										  cv::Point xThresholds, cv::Point yThresholds)
{
    const double GROUP_EPS = 0.2;


    CV_Assert( scaleFactor > 1 && image.depth() == CV_8U );

    if( empty() )
        return;

    if( isOldFormatCascade() )
    {
       std::cout << "Old cascade format unsupported. Exiting...\n";
	   exit(-1);
    }

    objects.clear();

    if (!maskGenerator.empty()) {
        maskGenerator->initializeMask(image);
    }


    if( maxObjectSize.height == 0 || maxObjectSize.width == 0 )
        maxObjectSize = image.size();

    Mat grayImage = image;
    if( grayImage.channels() > 1 )
    {
        Mat temp;
        cvtColor(grayImage, temp, CV_BGR2GRAY);
        grayImage = temp;
    }

    Mat imageBuffer(image.rows + 1, image.cols + 1, CV_8U);
    vector<Rect> candidates;

    for( double factor = 1; ; factor *= scaleFactor )
    {
        Size originalWindowSize = getOriginalWindowSize();

        Size windowSize( cvRound(originalWindowSize.width*factor), cvRound(originalWindowSize.height*factor) );
        Size scaledImageSize( cvRound( grayImage.cols/factor ), cvRound( grayImage.rows/factor ) );
        Size processingRectSize( scaledImageSize.width - originalWindowSize.width, scaledImageSize.height - originalWindowSize.height );

        if( processingRectSize.width <= 0 || processingRectSize.height <= 0 )
            break;
        if( windowSize.width > maxObjectSize.width || windowSize.height > maxObjectSize.height )
            break;
        if( windowSize.width < minObjectSize.width || windowSize.height < minObjectSize.height )
            continue;

        Mat scaledImage( scaledImageSize, CV_8U, imageBuffer.data );
        resize( grayImage, scaledImage, scaledImageSize, 0, 0, CV_INTER_LINEAR );

		// !HERE
		// modified step computation
		int yStep, xStep;
		if( getFeatureType() == cv::FeatureEvaluator::HOG )
		{
			yStep = 4;		// RASW is not parameterized for HOG, results are not certain.
			xStep=4;
		}
		else
		{
			//yStep = factor > 2. ? 1 : 2;		// these steps will define minimal X and Y steps for RASW to speed-up
			//xStep = factor > 2. ? 1 : 2;		// will lower performance though; keep it = 1 if you need performance.
			xStep=1;
			yStep=1;
		}

		int stripCount;
		cv::Point stripSize;

		const int PTS_PER_THREAD = 1000;
		/*stripCount = (processingRectSize.width*processingRectSize.height + PTS_PER_THREAD/2)/PTS_PER_THREAD;	// worst case : steps =1
		stripCount = std::min(std::max(stripCount, 1), 100);
		stripSize = cv::Point(processingRectSize.width, processingRectSize.height);*/

		stripCount = ((processingRectSize.width)*(processingRectSize.height) + PTS_PER_THREAD/2)/PTS_PER_THREAD;
        stripCount = std::min(std::max(stripCount, 1), 100);
        stripSize = cv::Point(((processingRectSize.width + stripCount - 1)/stripCount), ((processingRectSize.height + stripCount - 1)/stripCount));
			
		if( !detectSingleScale( scaledImage, stripCount, processingRectSize, stripSize, xStep, yStep, factor, candidates,
			xThresholds, yThresholds) )
			break;
		/////////////////////////////////
    }


    objects.resize(candidates.size());
    std::copy(candidates.begin(), candidates.end(), objects.begin());

    groupRectangles( objects, minNeighbors, GROUP_EPS );
}


// modified for integrating xStep
// used for RASW
bool RASW_extended_VJ::detectSingleScale( const Mat& image, int stripCount, Size processingRectSize,
                                           cv::Point stripSize, int xStep, int yStep, double factor, vector<Rect>& candidates, cv::Point xThresholds, cv::Point yThresholds )
{
    if( !featureEvaluator->setImage( image, data.origWinSize ) )
        return false;

#if defined (LOG_CASCADE_STATISTIC)
    logger.setImage(image);
#endif

    Mat currentMask;
    if (!maskGenerator.empty()) {
        currentMask=maskGenerator->generateMask(image);
    }

    vector<Rect> candidatesVector;
    vector<int> rejectLevels;
    vector<double> levelWeights;
    Mutex mtx;

    parallel_for_(Range(0, stripCount),RASW_extended_VJInvoker( *this, processingRectSize, stripSize, xStep, yStep, factor,
    candidatesVector, xThresholds, yThresholds, currentMask, &mtx));

    candidates.insert( candidates.end(), candidatesVector.begin(), candidatesVector.end() );

#if defined (LOG_CASCADE_STATISTIC)
    logger.write();
#endif

    return true;
}

bool RASW_extended_VJ::Data::read(const FileNode &root)
{
    static const float THRESHOLD_EPS = 1e-5f;

    // load stage params
    string stageTypeStr = (string)root[CC_STAGE_TYPE];
    if( stageTypeStr == CC_BOOST )
        stageType = BOOST;
    else
        return false;

    string featureTypeStr = (string)root[CC_FEATURE_TYPE];
    if( featureTypeStr == CC_HAAR )
        featureType = FeatureEvaluator::HAAR;
    else if( featureTypeStr == CC_LBP )
        featureType = FeatureEvaluator::LBP;
    else if( featureTypeStr == CC_HOG )
        featureType = FeatureEvaluator::HOG;

    else
        return false;

    origWinSize.width = (int)root[CC_WIDTH];
    origWinSize.height = (int)root[CC_HEIGHT];
    CV_Assert( origWinSize.height > 0 && origWinSize.width > 0 );

    isStumpBased = (int)(root[CC_STAGE_PARAMS][CC_MAX_DEPTH]) == 1 ? true : false;

    // load feature params
    FileNode fn = root[CC_FEATURE_PARAMS];
    if( fn.empty() )
        return false;

    ncategories = fn[CC_MAX_CAT_COUNT];
    int subsetSize = (ncategories + 31)/32,
        nodeStep = 3 + ( ncategories>0 ? subsetSize : 1 );

    // load stages
    fn = root[CC_STAGES];
    if( fn.empty() )
        return false;

    stages.reserve(fn.size());
    classifiers.clear();
    nodes.clear();

    FileNodeIterator it = fn.begin(), it_end = fn.end();

    for( int si = 0; it != it_end; si++, ++it )
    {
        FileNode fns = *it;
        Stage stage;
        stage.threshold = (float)fns[CC_STAGE_THRESHOLD] - THRESHOLD_EPS;
        fns = fns[CC_WEAK_CLASSIFIERS];
        if(fns.empty())
            return false;
        stage.ntrees = (int)fns.size();
        stage.first = (int)classifiers.size();
        stages.push_back(stage);
        classifiers.reserve(stages[si].first + stages[si].ntrees);

        FileNodeIterator it1 = fns.begin(), it1_end = fns.end();
        for( ; it1 != it1_end; ++it1 ) // weak trees
        {
            FileNode fnw = *it1;
            FileNode internalNodes = fnw[CC_INTERNAL_NODES];
            FileNode leafValues = fnw[CC_LEAF_VALUES];
            if( internalNodes.empty() || leafValues.empty() )
                return false;

            DTree tree;
            tree.nodeCount = (int)internalNodes.size()/nodeStep;
            classifiers.push_back(tree);

            nodes.reserve(nodes.size() + tree.nodeCount);
            leaves.reserve(leaves.size() + leafValues.size());
            if( subsetSize > 0 )
                subsets.reserve(subsets.size() + tree.nodeCount*subsetSize);

            FileNodeIterator internalNodesIter = internalNodes.begin(), internalNodesEnd = internalNodes.end();

            for( ; internalNodesIter != internalNodesEnd; ) // nodes
            {
                DTreeNode node;
                node.left = (int)*internalNodesIter; ++internalNodesIter;
                node.right = (int)*internalNodesIter; ++internalNodesIter;
                node.featureIdx = (int)*internalNodesIter; ++internalNodesIter;
                if( subsetSize > 0 )
                {
                    for( int j = 0; j < subsetSize; j++, ++internalNodesIter )
                        subsets.push_back((int)*internalNodesIter);
                    node.threshold = 0.f;
                }
                else
                {
                    node.threshold = (float)*internalNodesIter; ++internalNodesIter;
                }
                nodes.push_back(node);
            }

            internalNodesIter = leafValues.begin(), internalNodesEnd = leafValues.end();

            for( ; internalNodesIter != internalNodesEnd; ++internalNodesIter ) // leaves
                leaves.push_back((float)*internalNodesIter);
        }
    }

    return true;
}

bool RASW_extended_VJ::read(const FileNode& root)
{
    if( !data.read(root) )
        return false;

    // load features
    featureEvaluator = FeatureEvaluator::create(data.featureType);
    FileNode fn = root[CC_FEATURES];
    if( fn.empty() )
        return false;

    return featureEvaluator->read(fn);
}

bool RASW_extended_VJ::isOldFormatCascade() const
{
    return !oldCascade.empty();
}

int RASW_extended_VJ::getFeatureType() const
{
    return featureEvaluator->getFeatureType();
}

Size RASW_extended_VJ::getOriginalWindowSize() const
{
    return data.origWinSize;
}

bool RASW_extended_VJ::setImage(const Mat& image)
{
    return featureEvaluator->setImage(image, data.origWinSize);
}



RASW_extended_VJ::~RASW_extended_VJ()
{

}

