#include "Detectors/Detectors.h"



DET::cascadeDetector::cascadeDetector()
{
	findBiggest=false;
	scaleFactor=1.10;
	minNeighbors=3;
	minSize=cv::Point();
	maxSize=cv::Point();
	manualInit=false;
}

DET::cascadeDetector::cascadeDetector(const cv::Size & minDetectionSize, const cv::Size & maxDetectionSize, const bool & onlyOneObject, const double & iterationScaleFactor, const int & minimumNeighbors)
{
	minSize=minDetectionSize;
	maxSize=maxDetectionSize;
	findBiggest=onlyOneObject;
	scaleFactor=iterationScaleFactor;
	minNeighbors=minimumNeighbors;
	manualInit=true;
	usesRASW=false;
}

DET::cascadeDetector::cascadeDetector(const cv::Size & minDetectionSize, const cv::Size & maxDetectionSize, const bool & onlyOneObject, const double & iterationScaleFactor, const int & minimumNeighbors, const cv::Point & xThresholds, const cv::Point & yThresholds)
{
	minSize=minDetectionSize;
	maxSize=maxDetectionSize;
	findBiggest=onlyOneObject;
	scaleFactor=iterationScaleFactor;
	minNeighbors=minimumNeighbors;
	manualInit=true;
	usesRASW=true;
	xThreshs=xThresholds;
	yThreshs=yThresholds;
}


bool DET::cascadeDetector::detect(const cv::Mat & in, std::vector<cv::Rect> & ROIs)
{
	cv::Mat grayImage;
	if(in.channels()>1)
		cv::cvtColor(in, grayImage, CV_BGR2GRAY);
	else
		grayImage=in.clone();
	cv::equalizeHist(grayImage, grayImage);

	if(manualInit && modelIsLoaded && !usesRASW)
		VJmodel.detectMultiScale(grayImage, ROIs, scaleFactor, minNeighbors, 0, minSize, maxSize);
	else if(!manualInit && modelIsLoaded && !usesRASW)
		VJmodel.detectMultiScale(grayImage, ROIs);
	else if(manualInit && modelIsLoaded && usesRASW)
		VJmodel_RASW.detectMultiScale(grayImage, ROIs, scaleFactor, minNeighbors, 0, minSize, maxSize, xThreshs, yThreshs);
	else if(!manualInit && modelIsLoaded && usesRASW)
	{
		std::cout << "Manual initialisation of all parameters through the RASW constructor is mandatory. Exiting...\n";
		exit(-1);
	}
	else if(!modelIsLoaded)
	{
		std::cout << "No cascade model loaded\n";
		exit(-1);
	}

	if(findBiggest)
	{
		std::cout << "Forget finding the biggest object, this is not implemented yet.\n";
	}
	
	if(ROIs.size()>0)
		return true;
	else 
		return false;
}

bool DET::cascadeDetector::detectAndDisplay(const cv::Mat & in, std::vector<cv::Rect> & ROIs, cv::Mat & out)
{
	static UTI::timer timer;
	timer.start();
	bool detected=detect(in, ROIs);

	out=in.clone();
	timer.showFPS(out, 10);
	for(int i=0; i<ROIs.size(); i++)
	{
		cv::rectangle(out, ROIs[i], cv::Scalar(0,0,255.0/(i+1)), 2);
		//std::cout << "Rectangle " << i << " has a size of " << ROIs[i].width << "x" << ROIs[i].height << "\n";
	}

	cv::imshow("detectAndDisplay", out);
	cv::waitKey(1);
	return detected;
}

bool DET::cascadeDetector::loadModel(const std::string & model)
{
	if(usesRASW)
		modelIsLoaded=VJmodel_RASW.load(model);
	else
		modelIsLoaded=VJmodel.load(model);
	return modelIsLoaded;
}


DET::cascadeDetector::~cascadeDetector()
{

}

/*
DET::HOG_SVMDetector::HOG_SVMDetector()
{
	// HOG detector and descriptor parameters - default for IARC bots
	winSize=cv::Size(48, 48);		// window size 
	cv::Size blockSize=cv::Size(16,16);		// block size
	cv::Size blockStride=cv::Size(8,8);		// typically half of the block size
	cv::Size cellSize=cv::Size(8,8);		// cell size
	windowStride=cv::Size(8,8);	// for moving the sliding window
	int nBins=9;							// orientation bins
	double thresholdL2HYS=0.2;				// clipping value. 0.2 for pedestrian as published by Dalal et al.
	int defaultNLevels=64;					// Maximum number of detection window increases. Should be 64
	bool gammaPreProcessing=false;			// Authors claim it is unnecessary
	HOGmodel=cv::HOGDescriptor::HOGDescriptor(winSize, blockSize, blockStride, cellSize, nBins, 1, -1, 0, thresholdL2HYS, gammaPreProcessing, defaultNLevels);
	hitThresh=0.5;							// default, may need tuning
}

DET::HOG_SVMDetector::HOG_SVMDetector(const double & hitThreshold, const cv::Size & WinSize, const cv::Size & WinStride, const cv::Size & BlockSize, const cv::Size & CellSize, const int & binAmount, const double & clippingValue, const int & NLevels, const bool & useGammaPreProcessing)
{
	// HOG detector and descriptor parameters - default for IARC bots
	winSize=WinSize;							// window size 
	cv::Size blockSize=BlockSize;						// block size
	cv::Size blockStride=CellSize;						// typically half of the block size
	cv::Size cellSize=CellSize;							// cell size
	windowStride=WinStride;								// for moving the sliding window
	int nBins=binAmount;								// orientation bins
	double thresholdL2HYS=clippingValue;				// clipping value. 0.2 for pedestrian as published by Dalal et al.
	int defaultNLevels=NLevels;							// Maximum number of detection window increases. Should be 64
	bool gammaPreProcessing=useGammaPreProcessing;		// Authors claim it is unnecessary
	HOGmodel=cv::HOGDescriptor::HOGDescriptor(winSize, blockSize, blockStride, cellSize, nBins, 1, -1, 0, thresholdL2HYS, gammaPreProcessing, 64);
	hitThresh=hitThreshold;
}

void DET::HOG_SVMDetector::trainSVM(const std::string & inputIndexFile, const std::string & outputVectorFile)
{
	// initialize SVM and its output
	SVMLight::SVMTrainer svm("HOGfeatures.dat");

	// let's read the index file
	// structure should be	0 negative01.png
	//						1 positive01.png
	//						1 positive02.png
	//						0 negative02.png
	// and so on
	UTI::fileReading index;
	index.openFile(inputIndexFile);
	int posCount=0, negCount=0;
	std::string content;
	std::cout << "Starting the SVM-HOG training process.\nGathering data from index file...\n";
	while(index.readNextLine(content))	// get label
	{
		// get label and picture
		int label=atoi(content.c_str());
		if(label==0)
			negCount++;
		else if(label==1)
			posCount++;
		else
		{
			std::cout << "\nUse of label " << label << " is not authorized - ignoring associated picture\n";
			index.readNextLine(content);	// skip the picture
			break;
		}
		index.readNextLine(content);	
		cv::Mat img=cv::imread(content, CV_LOAD_IMAGE_GRAYSCALE);
		cv::resize(img, img, winSize);

		// compute HOG
		std::vector<float> featureVector;
		HOGmodel.compute(img, featureVector, windowStride, cv::Size(0, 0));

		// write to file for SVM training
		if(label==0)
			svm.writeFeatureVectorToFile(featureVector, false);
		else if(label==1)
			svm.writeFeatureVectorToFile(featureVector, true);
	}

	// now do the actual training
	std::cout	<< "... done\n"
				<< posCount << " positive and "
                << negCount << " negative samples used\n"
				<< "Training SVM...";
	svm.trainAndSaveModel(outputVectorFile);
	std::cout   << "done\nSVM saved to " << outputVectorFile;
}

bool DET::HOG_SVMDetector::loadModel(const std::string & inputVectorFile)
{
	SVMLight::SVMClassifier svm(inputVectorFile);
    std::vector<float> descriptorVector = svm.getDescriptorVector();
    HOGmodel.setSVMDetector(descriptorVector);
	return !descriptorVector.empty();
}

bool DET::HOG_SVMDetector::detect(const cv::Mat & in, std::vector<cv::Rect> & ROIs)
{
	// convert to grayscale
	cv::Mat grayImage;
	cv::cvtColor(in, grayImage, CV_BGR2GRAY);

	// detect
	HOGmodel.detectMultiScale(grayImage, ROIs, hitThresh, windowStride, cv::Size(0,0), 1.20, 2);

	if(ROIs.size()>0)
		return true;
	else 
		return false;
}

bool DET::HOG_SVMDetector::detectAndDisplay(const cv::Mat & in, std::vector<cv::Rect> & ROIs, cv::Mat & out)
{
	static UTI::timer timer;
	timer.start();
	bool detected=detect(in, ROIs);

	out=in.clone();
	timer.showFPS(out, 10);
	for(int i=0; i<ROIs.size(); i++)
	{
		cv::rectangle(out, ROIs[i], cv::Scalar(0,0,255.0/(i+1)), 2);
		//std::cout << "Rectangle " << i << " has a size of " << ROIs[i].width << "x" << ROIs[i].height << "\n";
	}

	cv::imshow("detectAndDisplay", out);
	cv::waitKey(1);
	return detected;
}

DET::HOG_SVMDetector::~HOG_SVMDetector()
{

}

*/
