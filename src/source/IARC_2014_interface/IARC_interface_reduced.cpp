#include "IARC_2014_interface/IARC_interface_reduced.h"

//////////// GRID NODE ///////////////////////
///////////////////////////////
// PRIVATE FUNCTIONS
bool IARC14_grid_node::undistortFrame(const cv::Mat & frame, cv::Mat & out)
{
	return camObj.run(frame, out);
}


///////////////////////////////
// INITIALIZATIONS
void IARC14_grid_node::initializeCameraParameters(const cv::Size & frameSize, const cv::Mat & cameraMatrix, 
		const cv::Mat & distorsionCoefficients)
{
	camObj.initialize(frameSize, cameraMatrix, distorsionCoefficients);
}

void IARC14_grid_node::initializeCameraParameters(std::string configFile)
{
    //From aruco
    cv::FileStorage fs(configFile, cv::FileStorage::READ);
    int w=-1,h=-1;
    cv::Mat MCamera,MDist;
    cv::Mat CameraMatrix, Distorsion;
    cv::Size CamSize;

    fs["image_width"] >> w;
    fs["image_height"] >> h;
    fs["distortion_coefficients"] >> MDist;
    fs["camera_matrix"] >> MCamera;

    if (MCamera.cols==0 || MCamera.rows==0)throw cv::Exception(9007,"File :"+configFile+" does not contains valid camera matrix","CameraParameters::readFromXML",__FILE__,__LINE__);
    if (w==-1 || h==0)throw cv::Exception(9007,"File :"+configFile+" does not contains valid camera dimensions","CameraParameters::readFromXML",__FILE__,__LINE__);

    if (MCamera.type()!=CV_32FC1) MCamera.convertTo(CameraMatrix,CV_32FC1);
    else CameraMatrix=MCamera;

    if (MDist.total()<4) throw cv::Exception(9007,"File :"+configFile+" does not contains valid distortion_coefficients","CameraParameters::readFromXML",__FILE__,__LINE__);
    //convert to 32 and get the 4 first elements only
    cv::Mat mdist32;
    MDist.convertTo(mdist32,CV_32FC1);
    Distorsion.create(1,4,CV_32FC1);
    for (int i=0;i<4;i++)
        Distorsion.ptr<float>(0)[i]=mdist32.ptr<float>(0)[i];

    CamSize.width=w;
    CamSize.height=h;



    //Jefs
    camObj.initialize(CamSize, CameraMatrix, Distorsion);


    return;
}

void IARC14_grid_node::initializeGridModule(const cv::Size workingWindowSize, const cv::Vec3i BGR_refinementThresholdsForBG, 
		const double maskMaxWhiteRatio, const cv::Size DT_cellCountXY, const int RANSCAC_MAXIMUM_ITERATIONS, 
		const int RANSAC_MAXIMUM_INLIER_DISTANCE, const int RANSAC_MINIMUM_INLIER_COUNT, 
		const int & IntersectionClusteringRadius, const int & Y_channel_treshold)
{
	intersectionClusteringRadius=IntersectionClusteringRadius;
    gridObj=GridLineFinder(workingWindowSize, BGR_refinementThresholdsForBG, maskMaxWhiteRatio, DT_cellCountXY, RANSCAC_MAXIMUM_ITERATIONS, RANSAC_MAXIMUM_INLIER_DISTANCE, RANSAC_MINIMUM_INLIER_COUNT,Y_channel_treshold);
}


//////////////////////////////
// RUNNING
int IARC14_grid_node::run(const cv::Mat & frame, cv::Mat & undistortedFrameOutput, std::vector<cv::Point> & intersections)
{
	///////////////////////////
	// inits
	orgFrame=frame.clone();
	intersections.clear();

	///////////////////////////
	// first undistort
	if(!undistortFrame(orgFrame, undistortedFrame))
		return -1;
	else
		undistortedFrameOutput=undistortedFrame.clone();

	//////////////////////////
	// then find intersections
    std::vector<cv::Point> tmpIntersections;
	if(gridObj.run(undistortedFrame, tmpIntersections))
	{
		gridObj.intersectionClustering(tmpIntersections, intersections, intersectionClusteringRadius);
        return 0;
	}
    else
        return -2;
}

void IARC14_grid_node::showResults(cv::Mat & picture, const std::vector<cv::Point> & intersections)
{
    for(int i=0; i<intersections.size(); i++)
            {
                cv::circle(picture, intersections[i], 7, cv::Scalar(255,0,127), -1);
                //cv::circle(picture, intersections[i], intersectionClusteringRadius,    cv::Scalar(255,0,127), 1);
            }

}



//////////// BOT NODE ///////////////////////
///////////////////////////////
// PRIVATE FUNCTIONS
bool IARC14_bot_node::undistortFrame(const cv::Mat & frame, cv::Mat & out)
{
	return camObj.run(frame, out);
}

bool IARC14_bot_node::segmentFrame(const cv::Mat & in, cv::Mat & out)
{
	return csvmObj.run(in, out);
}


///////////////////////////////
// INITIALIZATIONS
void IARC14_bot_node::initializeCameraParameters(const cv::Size & frameSize, const cv::Mat & cameraMatrix, 
		const cv::Mat & distorsionCoefficients)
{
	camObj.initialize(frameSize, cameraMatrix, distorsionCoefficients);
}


void IARC14_bot_node::initializeCameraParameters(std::string configFile)
{
    //From aruco
    cv::FileStorage fs(configFile, cv::FileStorage::READ);
    int w=-1,h=-1;
    cv::Mat MCamera,MDist;
    cv::Mat CameraMatrix, Distorsion;
    cv::Size CamSize;

    fs["image_width"] >> w;
    fs["image_height"] >> h;
    fs["distortion_coefficients"] >> MDist;
    fs["camera_matrix"] >> MCamera;

    if (MCamera.cols==0 || MCamera.rows==0)throw cv::Exception(9007,"File :"+configFile+" does not contains valid camera matrix","CameraParameters::readFromXML",__FILE__,__LINE__);
    if (w==-1 || h==0)throw cv::Exception(9007,"File :"+configFile+" does not contains valid camera dimensions","CameraParameters::readFromXML",__FILE__,__LINE__);

    if (MCamera.type()!=CV_32FC1) MCamera.convertTo(CameraMatrix,CV_32FC1);
    else CameraMatrix=MCamera;

    if (MDist.total()<4) throw cv::Exception(9007,"File :"+configFile+" does not contains valid distortion_coefficients","CameraParameters::readFromXML",__FILE__,__LINE__);
    //convert to 32 and get the 4 first elements only
    cv::Mat mdist32;
    MDist.convertTo(mdist32,CV_32FC1);
    Distorsion.create(1,4,CV_32FC1);
    for (int i=0;i<4;i++)
        Distorsion.ptr<float>(0)[i]=mdist32.ptr<float>(0)[i];

    CamSize.width=w;
    CamSize.height=h;



    //Jefs
    camObj.initialize(CamSize, CameraMatrix, Distorsion);


    return;
}



void IARC14_bot_node::initializeColorSVM_segmentation(const std::string & csvm_model, const std::string & csvm_scaleValues, 
		const cv::Size & workSize, const int & svm_type, const int & colorspace)
{
	csvmObj.initialize(csvm_model, csvm_scaleValues, workSize, svm_type, colorspace);
}

void IARC14_bot_node::initializeDetectionModule(const std::string & modelPathAndName, 
	const cv::Size & minDetectionRectangleSize, const cv::Size & maxDetectionRectangleSize, const cv::Size & xThresholdsRASW, 
	const cv::Size & yThresholdsRASW, const double & scaleFactor, const int & minNeighbors, const int & successiveFramesToDetect, 
	const int & successiveFramesToTrack, const int & maxBotsToTrack)
{
	/////////////////////////////////////
	// init the bot detector & tracker
	FramesToDetect=successiveFramesToDetect;
	FramesToTrack=successiveFramesToTrack;
	numBotsToTrack=maxBotsToTrack;
	if(!vjObj.initialize(modelPathAndName, minDetectionRectangleSize, maxDetectionRectangleSize, xThresholdsRASW, yThresholdsRASW, scaleFactor, minNeighbors))
	{
		std::cout << "Invalid VJ model\n";
		exit(-1);
	}
	trackingObj.initialize(numBotsToTrack);
}

void IARC14_bot_node::initializeIdentificationModule(const cv::Scalar & whiteThresholds, 
	const cv::Scalar & greenThresholds, const cv::Scalar & redThresholds, const double & HoughMin_voteInputHeight_ratio, 
	const int & HoughMinAngle_degrees, const int & HoughMaxAngle_degrees, const double & HoughVerticalROI_extensionFactor,
	bool useColor, bool useHough)
{
	// init the bot identifier
	// !!! these values are in RGB (for identification purposes)
	IFFObj.initialize(whiteThresholds, redThresholds, greenThresholds, HoughMin_voteInputHeight_ratio, HoughMinAngle_degrees, 
		HoughMaxAngle_degrees, HoughVerticalROI_extensionFactor, useColor, useHough);
}

void IARC14_bot_node::initializeLabellingModule(const int & maxAllowedPositionDrift, const bool & useTypeMatching, const int & maxSkippedFrames)
{
	labelObj.init(maxAllowedPositionDrift, useTypeMatching, maxSkippedFrames);
}


//////////////////////////////
// RUNNING
int IARC14_bot_node::run(const cv::Mat & frame, cv::Mat & undistortedFrameOutput, std::vector<cv::Rect> & botBoundingBoxes, std::vector<int> & botTypes, std::vector<int> & botLabels)
{
	///////////////////////////
	// state of the node
	static bool detected=false;
	static bool tracking=false;
	static int trackingFrameCounter=0;
	bool csvmDone=false;
	int success=0;

	///////////////////////////
	// inits
	orgFrame=frame.clone();
	botBoundingBoxes.clear();
	botTypes.clear();
	botLabels.clear();

	///////////////////////////
	// first undistort
	if(!undistortFrame(orgFrame, undistortedFrame))
		success=-1;
	else
		undistortedFrameOutput=undistortedFrame.clone();

	///////////////////////////
	// then perform tracking (if previous detection)
	if(detected && tracking && FramesToTrack>0 && success==0)
	{	
		if(tracking)	// track the objects
		{
			std::vector<cv::Rect> trackedROIs;
			tracking=trackingObj.track(undistortedFrame, botROIs, trackedROIs, botOrientations);
			if(tracking)
			{
				//std::cout << "Tracking " << ROIs.size() << " bots.\n";
				// make sure every tracked ROI is valid and add it to ROIs
				botROIs.clear();
				for(int i=0; i<trackedROIs.size(); i++)
					if(UTI::images::checkROIValidity(undistortedFrame.size(), trackedROIs[i]))
						botROIs.push_back(trackedROIs[i]);
			}
			else	// all trackers failed, stop the tracking, get the detection going
			{
				trackingObj.stopTracking();
				detected=false;
			}
			trackingFrameCounter--;
		}

		if(trackingFrameCounter==0)	// not allowed to track more successive frames, stop the tracking, get the detection going
		{
			tracking=false;
			trackingObj.stopTracking();
			detected=false;
		}
	}
	
	///////////////////////////
	// detection (if not tracking)
	if(success==0)
	{
		if(!detected || !tracking)
		{
			detected=vjObj.run(undistortedFrame, botROIs);
		}
		else 
		{/*NOTHING*/}
	}

	// Here we should have bot rois from detection or tracking
	if(botROIs.empty())
		success= -3;
	else
		botBoundingBoxes=std::vector<cv::Rect>(botROIs);

	///////////////////////////
	// tracker init for next frame
	if(detected && !tracking && FramesToTrack>0 && success==0)
	{	
		if(!tracking)	// init with VJ detections
		{
			if(!csvmDone)		// segment the picture for initializing the tracker
				csvmDone=segmentFrame(undistortedFrame, csvmMask);
			if(!csvmDone)
				success= -2;
			tracking=trackingObj.computeReference(undistortedFrame, csvmMask, botROIs);
			trackingFrameCounter=FramesToTrack;
		}
		if(!tracking)	// if init failed, ignore the tracking process, back to VJ
				detected=false;	
	}
	
	///////////////////////////
	// IFF (if found bot region)
	if(!botROIs.empty())
	{
		if(!csvmDone)	// segment the picture for providing color clues
			csvmDone=segmentFrame(undistortedFrame, csvmMask);
		if(!csvmDone)
			success= -2;
		if(success==-2)
		{}
		else
			IFFObj.run(undistortedFrame, csvmMask, botROIs, botTypes);
	}
	if(botTypes.size()!=botROIs.size())
		success= -4;

	///////////////////////////
	// Bot Label Assignment
	labelObj.run(botROIs, botTypes, botLabels);

	return success;
}

void IARC14_bot_node::showResults(cv::Mat & picture, const std::vector<cv::Rect> & botBoundingBoxes, const std::vector<int> & botLabels)
{
    // these values are in RGB (for drawing purposes)
    static cv::Scalar unknownColor=cv::Scalar(0,255,255);
    static cv::Scalar poleColor=cv::Scalar(255,255,255);
    static cv::Scalar targetColor=cv::Scalar(77,234,60);

    int botCount=0, poleCount=0, unknownCount=0;

    for(int i=0; i<botBoundingBoxes.size(); i++)
    {
        if(botLabels.empty())
        {
            cv::rectangle(picture, botBoundingBoxes[i], unknownColor, 1);
            unknownCount+=botBoundingBoxes.size();
        }
        else
        {
            // increment botcount/polecount if a bot/pole was identified
            if(botLabels[i]<0)
            {
                poleCount++;
                cv::rectangle(picture, botBoundingBoxes[i], poleColor, 1);
            }
            else if(botLabels[i]>0)
            {
                botCount++;
                cv::rectangle(picture, botBoundingBoxes[i], targetColor, 1);
            }
            else
            {
                unknownCount++;
                cv::rectangle(picture, botBoundingBoxes[i], unknownColor, 1);
            }
        }
    }

    // not working, requires itoa
    /*char buf[10];
    std::string botText=itoa(botCount, buf, 10);
    botText+=" BOT(S)";
    UTI::images::putText(picture, cv::Point(picture.size().width*0.5, picture.size().height/15), botText);
    std::string poleText=itoa(poleCount, buf, 10);
    poleText+=" POLE(S)";
    UTI::images::putText(picture, cv::Point(picture.size().width*0.75, picture.size().height/15), poleText);
    std::string unknownText=itoa(unknownCount, buf, 10);
    unknownText+=" ???";
    UTI::images::putText(picture, cv::Point(picture.size().width*0.35, picture.size().height/15), unknownText);*/

}
