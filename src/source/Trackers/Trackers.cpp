#pragma warning(disable : 4006)

#include "Trackers/Trackers.h"

///////////////////////////////////////
// KALMAN
///////////////////////////////////////
TRK::Kalman::Kalman() {}

void TRK::Kalman::setTransitionMatrixWeights(const double & Vx_weight, const double & Vy_weight, const double & Ax_weight, const double & Ay_weight)
{
	transitionMatrix=cv::Mat::eye(6,6, CV_32FC1);
	if(Vx_weight>=0)
		transitionMatrix.at<float>(0,2)=Vx_weight;
	else
		transitionMatrix.at<float>(0,2)=1;
	if(Ax_weight>=0)
	{
		transitionMatrix.at<float>(0,4)=Ax_weight;
		transitionMatrix.at<float>(2,4)=Ax_weight*2;
	}
	else
	{
		transitionMatrix.at<float>(0,4)=0.5;
		transitionMatrix.at<float>(2,4)=1;
	}
	if(Vy_weight>=0)
		transitionMatrix.at<float>(1,3)=Vy_weight;
	else
		transitionMatrix.at<float>(1,3)=1;
	if(Ay_weight>=0)
	{
		transitionMatrix.at<float>(1,5)=Ay_weight;
		transitionMatrix.at<float>(3,5)=Ay_weight*2;
	}
	else
	{
		transitionMatrix.at<float>(1,5)=0.5;
		transitionMatrix.at<float>(3,5)=1;
	}

	std::cout << "Kalman transition matrix is : \n" << transitionMatrix << "\n";
}

void TRK::Kalman::init(const int & stateDims, const int & measurementDims, const int & controlDims)
{
	KF.init(stateDims, measurementDims, controlDims, CV_32F);	
	cv::setIdentity(KF.measurementMatrix);
	cv::setIdentity(KF.processNoiseCov, cv::Scalar::all(1e-5));
    cv::setIdentity(KF.measurementNoiseCov, cv::Scalar::all(1e-1));
    cv::setIdentity(KF.errorCovPost, cv::Scalar::all(1));

	if(transitionMatrix.empty())
	{
		std::cout << "Warning : empty Kalman filter transition matrix. Call setTransitionMatrixWeights() before calling init() !\nTransition matrix will be initialized with default values.\n";
		setTransitionMatrixWeights(-1, -1, -1, -1);
	}
	KF.transitionMatrix=transitionMatrix.clone();

	first=true;

	/*std::cout << "measurement mat " << KF.measurementMatrix.size() << std::endl;
	std::cout << "processNoiseCov " << KF.processNoiseCov.size() << std::endl;
	std::cout << "measurementNoiseCov " << KF.measurementNoiseCov.size() << std::endl;
	std::cout << "errorCovPost " << KF.errorCovPost.size() << std::endl;
	std::cout << "statePre " << KF.statePre.size() << std::endl;*/
}

void TRK::Kalman::firstUse(const cv::Point & state_start)
{
	KF.statePre.at<float>(0)=state_start.x;
	KF.statePre.at<float>(1)=state_start.y;
	KF.statePre.at<float>(2)=0;
	KF.statePre.at<float>(3)=0;
	first=false;
}

void TRK::Kalman::predict(const cv::Point & first_state, cv::Point & state_predicted)
{
	if(first)
		firstUse(first_state);

	cv::Mat prediction=KF.predict();
	state_predicted=cv::Point(prediction.at<float>(0),prediction.at<float>(1));
	//std::cout << "Kalman predicts : " << prediction.at<float>(0) << ", " << prediction.at<float>(1) << std::endl;
}

void TRK::Kalman::correct(const cv::Point & stateMeasurement, cv::Point & state_k)
{
	cv::Mat_<float> measurement(2,1);
	measurement(0)=stateMeasurement.x;
	measurement(1)=stateMeasurement.y;
	cv::Mat corrected_prediction=KF.correct(measurement);
	state_k=cv::Point(corrected_prediction.at<float>(0),corrected_prediction.at<float>(1));
}

TRK::Kalman::~Kalman() {}


///////////////////////////////////////
// CAMSHIFT
///////////////////////////////////////
bool TRK::CamShift::initializeTracking(const cv::Mat & trackerInitImage, const cv::Mat & mask)
{
	cv::Mat tmp;
	cv::cvtColor(trackerInitImage, tmp, CV_BGR2HSV);
	int nonZeroPix=cv::countNonZero(mask);
	if(nonZeroPix<mask.size().area()*0.2)
		computeHist(tmp, mask);
	else
		computeHist(tmp, cv::Mat());
	return true;
}

bool TRK::CamShift::performTracking(const cv::Mat & image, cv::Rect & searchWindowInit, cv::RotatedRect & objectLocation, double & objectOrientation)
{
	if(hist.empty())
	{
		std::cout << "Empty tracking histogram\n";
		return false;
	}
	if(searchWindowInit.size().area()<100)
	{
		std::cout << "CamShift initial search window < 100 square pixels\n";
		return false;
	}
	// convert to tracking colorspace (default HSV)
	cv::Mat input=image.clone();
	UTI::images::changeColorSpace(input, CV_RGB2HSV);

	// now let us track the object in the specified channel using the last known location
	computeBackProj(input);
	objectLocation = cv::CamShift(backproj, searchWindowInit, cv::TermCriteria( cv::TermCriteria::EPS | cv::TermCriteria::COUNT, 10, 1 ));
	objectOrientation = objectLocation.angle;
	return true;
}

void TRK::CamShift::computeHist(const cv::Mat & trackerInitImage, const cv::Mat & mask)
{
	// For H-S backProj
	 static int h_bins = 30; int s_bins = 32;
	 static int histSize[] = { h_bins, s_bins };
	 static  float h_range[] = { 0, 255 };
	 static float s_range[] = { 0, 255 };
	 static const float* ranges[] = { h_range, s_range };
	 static int channels[] = { 0, 1 };
	 /// Get the Histogram and normalize it
	 calcHist( &trackerInitImage, 1, channels, mask, hist, 2, histSize, ranges, true, false );
	 normalize( hist, hist, 0, 255, cv::NORM_MINMAX, -1, cv::Mat() );

	 // For H only backProj
	/* static int h_bins = 30;
	 static int histSize[] = { h_bins};
	 static  float h_range[] = { 0, 255 };
	 static const float* ranges[] = { h_range };
	 static int channels[] = { 0 };
	 /// Get the Histogram and normalize it
	 calcHist(&trackerInitImage, 1, channels, cv::Mat(), hist, 1, histSize, ranges, true, false );
	 normalize( hist, hist, 0, 255, cv::NORM_MINMAX, -1, cv::Mat() );*/
}

void TRK::CamShift::computeBackProj(const cv::Mat & picture)
{
	 static  float h_range[] = { 0, 255 };
	 // For H-S backProj
	 static float s_range[] = { 0, 255 };
	 static const float* ranges[] = { h_range, s_range };
	 static int channels[] = { 0, 1 };
	 // For H only backProj
	 //static const float* ranges[] = { h_range };
	 //static int channels[] = { 0};
	/// Get Backprojection
	 if(!hist.empty())
		calcBackProject( &picture, 1, channels, hist, backproj, ranges, 1, true );
	 else
		 std::cout << "BackProject histogram failure\n";
}
