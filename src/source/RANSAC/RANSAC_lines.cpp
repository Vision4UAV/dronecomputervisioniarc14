#pragma once

#include "RANSAC/RANSAC_lines.h"

bool computeLineIntersections(const std::vector<line_model> & lines, std::vector<cv::Point> & intersections, std::vector<float> & angle_delta)
{
	const float minangle=30;		// minimum delta angle (degree) between two lines for computing their intersection
	float A1, A2;
	bool nointersection;
	for(int i=0; i<lines.size(); i++)
	{
		for(int j=i+1; j<lines.size(); j++)		// intersection(i,j) == intersection(j,i), and
		{										// intersection(i,i) makes no sense
			nointersection=false;
			if(lines[i].vertical)	// vertical case
			{
				A1=89.9;	
				if(lines[j].vertical){nointersection=true;}	// no intersection
				else if(lines[j].horizontal){A2=0;}
				else{A2=std::atan(lines[j].A)*180/CV_PI;}
			}
			else if(lines[i].horizontal)	// horizontal case
			{
				A1=0;	
				if(lines[j].horizontal){nointersection=true;}	// no intersection
				else if(lines[j].vertical){A2=89.9;}
				else{A2=std::atan(lines[j].A)*180/CV_PI;}
			}
			else
			{
				A1=std::atan(lines[i].A)*180/CV_PI;
				if(lines[j].horizontal){A2=0;}	// no intersection
				else if(lines[j].vertical){A2=89.9;}
				else{A2=std::atan(lines[j].A)*180/CV_PI;}
			}

			float delta=std::abs(A2-A1);
			if(delta>90)
				delta=180-delta;
			if(delta<minangle)
				nointersection=true;

			if(nointersection){}
			else
			{
				// if the lines are interesting, compute their intersection
				cv::Point curr_intersection;
				float A11, A21;
				if(lines[i].vertical)
					A11=100000;
				else if(lines[i].horizontal)
					A11=0;
				else
					A11=lines[i].A;
				if(lines[j].vertical)
					A21=100000;
				else if(lines[j].horizontal)
					A21=0;
				else
					A21=lines[j].A;
				curr_intersection.x=(lines[j].C-lines[i].C)/(A11-A21);
				curr_intersection.y=curr_intersection.x*lines[i].A+lines[i].C;
				intersections.push_back(curr_intersection);
				angle_delta.push_back(delta);
			}
		}
	}

	if(intersections.empty())
		return false;
	else
		return true;
}

CASNAR::CASNAR()
{
	rng=cv::RNG(0);
}

////////////////////////////////////
// @in : vector of points assumed to be part of the grid
// @models : line models (y=Ax+C) returned by RANSAC
// @iterations : max number of RANSAC iterations
// @maxdist : maximum distance from a point to a line model to be considered an inlier
// @minvotes : minimum inliers a model needs to be considered valid
//
// Algorithm is optimized so as not to examine the same couple of points multiple times.
// Termination is effective upon computing @iterations iterations or when more than 90% of the 
// point couples have been examined.
////////////////////////////////////
void CASNAR::lines(const std::vector<cv::Point> & in, std::vector<line_model> & models, const int & iterations, const int & maxdist, const int & minvotes)
{
	// Mathematical part
	// a model of a line is
	// y=Ax+C	
	// for 2 pts (x1,y1) and (x2,y2)
	// A = (y2-y1)/(x2-x1)
	// C = y1-Ax1

	// inits
	data=std::vector<cv::Point>(in);
	Ti=maxdist;
	Tv=minvotes;
	int counter = iterations;
	cv::Point pt1, pt2;
	// init call index, 2D matrix containing at (i,j) the number of times the couple of points with indexes i and j 
	// have been called. Shouldn't be more than once, and (i,j) is the same as (j,i)
	cv::Mat callIdx=cv::Mat::zeros(in.size(), in.size(), CV_8UC1);
	double callIdxFillRatio=0;
	for(int y=0; y<callIdx.rows; y++)
	{
		callIdx.at<uchar>(y,y)=1;
		callIdxFillRatio+=1.0/(float)callIdx.size().area();
	}
	
	int i1, i2;
	while(counter>0 && callIdxFillRatio<=0.90)
	{
		// 1 - select 2 random points
		i1=rng.uniform(0, data.size());
		i2=rng.uniform(i1, data.size());
		while(callIdx.at<uchar>(i1,i2)>0)
		{
			i1=rng.uniform(0, data.size());
			i2=rng.uniform(i1, data.size());
		}
		callIdx.at<uchar>(i1,i2)=1;
		callIdx.at<uchar>(i2,i1)=1;
		callIdxFillRatio+=2.0/(float)callIdx.size().area();
		//std::cout << "iter " << iterations-counter << ", ratio = " << callIdxFillRatio << std::endl;
		pt1=in[i1];
		pt2=in[i2];

		// 2 - initialize the model
		compute_model(pt1, pt2);

		// 3 - Determine the set of data points within the inlier threshold
		model.votes=compute_inliers();

		// 4 - If we have enough inliers, recompute the model using the mean values 
		// and save it.
		if(model.votes>Tv){
			struct line_model currModel=fit_line();
			//std::cout << "2 pt model parameters: A=" << model.A << ", C=" << model.C << ", H" << model.horizontal << " V" << model.vertical << std::endl;
			//std::cout << "n pt model parameters: A=" << currModel.A << ", C=" << currModel.C << ", H" << currModel.horizontal << " V" << currModel.vertical << ", votes=" << currModel.votes << std::endl;
			//std::cout << "--------------------------\n";
			//struct line_model currModel=model;
			models.push_back(currModel);}
		else{}

		// 5 - restart till enough iterations.
		counter--;
	}
/*	if(callIdxFillRatio>0.90)
		std::cout << "RANSAC terminated by exhausting dataset.\n";
	else
		std::cout << "RANSAC terminated using " << callIdxFillRatio*100 << "% of the dataset.\n"; */
}

void CASNAR::secondPass(const std::vector<line_model> & models, std::vector<line_model> & important_models)
{
	// first, sort the models by vote count.
	// gives indexes of the models sorted by descending vote counts
	std::vector<int> index;
	sortModelsByDescendingVotes(models, index);

	// for each model in descending vote count order, 
	// -if the couple of generating points of the model have not been deleted from the data set :
	//    -> remove the inliers of this model from the data set (private variable @data)
	// -else, do not consider this model as valid.
	std::vector<int> deletedData=std::vector<int>(data.size(), 0);	// index of "deleted" data
	double deletedRatio=0;											// ratio of deleted points
	int currIndex;
	cv::Point pt1, pt2;
	std::vector<cv::Point>::iterator it;
	int ittoint1, ittoint2, ittoint3;
	int i=0;
	while(i<models.size() && deletedRatio<0.99)	// while there are models to examine or points remaining
	{
		currIndex=index[i];
		pt1=models[currIndex].pt1;
		it=std::find(data.begin(), data.end(), pt1);
		ittoint1=std::distance(data.begin(), it);
		//std::cout << ittoint1;
		if(deletedData[ittoint1]>0)	// if point has been deleted
		{/*std::cout << "("<< i <<") pt1 already deleted\n";*/}							// nothing
		else						// check pt2
		{
			pt2=models[currIndex].pt2;
			it=std::find(data.begin(), data.end(), pt2);
			ittoint2=std::distance(data.begin(), it);
			if(deletedData[ittoint2]>0)	// if point has been deleted
			{/*std::cout << "("<< i <<") pt2 already deleted\n";*/}							// nothing
			else						// OK, both points are valid
			{
				//std::cout << "("<< i <<") this model is important\n";
				important_models.push_back(models[currIndex]);		// store the model as important
				for(int j=0; j<models[currIndex].inliers.size(); j++)	// delete all inliers for future iterations
				{
					it=std::find(data.begin(), data.end(), models[currIndex].inliers[j]);
					ittoint3=std::distance(data.begin(), it);
					deletedData[ittoint3]=1;
					deletedRatio+=1.0/models.size();
				}
			}
		}
		i++;
	}
	/*if(deletedRatio>0.99)
		std::cout << "RANSAC (2) terminated by exhausting dataset.\n";
	else
		std::cout << "RANSAC (2) terminated using " << deletedRatio*100 << "% of the dataset.\n";*/
}

void CASNAR::sortModelsByDescendingVotes(const std::vector<line_model> & models, std::vector<int> & index)
{
	index=std::vector<int>(models.size(), 0);		// indexes of the sorted models
	std::vector<int> votes;	// sorted votes
	for(int i=0; i<models.size(); i++)
		votes.push_back(models[i].votes);
	std::vector<int>::iterator it;
	for(int i=0; i<models.size(); i++)
	{
		bool foundMax=false;
		for(int j=0; j<votes.size(); j++)
		{
			if(models[i].votes>votes[j])
			{
				it=votes.begin()+j;
				votes.insert(it, models[i].votes);
				votes.pop_back();
				it=index.begin()+j;
				index.insert(it, i);
				index.pop_back();
				foundMax=true;
				break;
			}
		}
		if(foundMax=false)	// last element
		{
			votes.pop_back();
			votes.push_back(models[i].votes);
			index.pop_back();
			index.push_back(i);
		}
	}
	/*for(int i=0; i<index.size(); i++)
		std::cout << index[i] << "(" << models[index[i]].votes << ") - ";*/
}

struct line_model CASNAR::fit_line()
{
	// least square regreesion
	struct line_model currModel;
	int n=model.inliers.size();
	currModel.votes=n;
	currModel.pt1=model.pt1;
	currModel.pt2=model.pt2;
	currModel.inliers=std::vector<cv::Point>(model.inliers);
	double term1=0, term2=0, term3=0, term4=0;
	for(int i=0; i<model.inliers.size(); i++)
	{
		term1+=model.inliers[i].x*model.inliers[i].y;
		term2+=model.inliers[i].x;
		term3+=model.inliers[i].y;
		term4+=model.inliers[i].x*model.inliers[i].x;
	}
	// ax+c=y
	currModel.A=(n*term1-term2*term3)/(n*term4-term2*term2);
	currModel.C=(term4*term3-term2*term1)/(n*term4-term2*term2);
	if(currModel.A==0)
		currModel.horizontal=1;
	else
		currModel.horizontal=0;
	currModel.vertical=0;

	return currModel;
}

int CASNAR::compute_inliers()
{
	model.inliers.clear();
	float norm=std::sqrt(model.A*model.A+1);
	float distance;
	for(int i=0; i<data.size(); i++)
	{
		if(model.vertical){	// vertical line. Compare X coordinates
			distance=std::abs(model.A-data[i].x);}
		else if(model.horizontal){	// horizontal line. Compare Y coordinates
			distance=std::abs(model.C-data[i].y);}
		else{
			distance=std::abs(model.A*data[i].x-data[i].y+model.C)/norm;}
		if(distance<Ti){	// closer than threshold for being inlier
			model.inliers.push_back(data[i]);}
	}

	return model.inliers.size();
}

line_model CASNAR::compute_model(const cv::Point & pt1, const cv::Point & pt2)
{
	// Mathematical part
	// a model of a line is
	// y=Ax+C	
	// for 2 pts (x1,y1) and (x2,y2)
	// A = (y2-y1)/(x2-x1)
	// C = y1-Ax1
	model.pt1=pt1;
	model.pt2=pt2;
	if(pt1.x==pt2.x)	// vertical line
	{
		model.A=pt1.x;
		model.C=0;
		model.vertical=true;
		model.horizontal=false;
	}
	else if(pt1.y==pt2.y)	// horizontal line
	{
		model.A=0;
		model.C=pt1.y;
		model.vertical=false;
		model.horizontal=true;
	}
	else
	{
		model.A=(float)(pt2.y-pt1.y)/(float)(pt2.x-pt1.x);
		model.C=pt1.y-model.A*pt1.x;
		model.vertical=false;
		model.horizontal=false;
	}

	return model;
}

CASNAR::~CASNAR(){}
