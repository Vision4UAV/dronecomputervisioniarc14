#pragma once

#include "SegmentationModule/SegmentationModule.h"


SMOD::COLORSVM::COLORSVM()
{
	manualConstructor();
}

void SMOD::COLORSVM::manualConstructor()		
{												
	colorSpaceSet=false;
	parametersSet=false;
	DataSet=false;
	scaleValuesComputed=false;
	SVMTrained=false;
}

void SMOD::COLORSVM::setSVMParameters(const int & kernelType)		// set the params here
{
	svmParameters.svm_type=CvSVM::C_SVC;
	svmParameters.kernel_type=kernelType;
	svmParameters.term_crit=cvTermCriteria(CV_TERMCRIT_ITER, 100, 1e-6);
	parametersSet=true;
}

void SMOD::COLORSVM::setColorspace(const int & colorspace)
{
	usedColorspace=colorspace;
	colorSpaceSet=true;
}

void SMOD::COLORSVM::addToTrainingData(const cv::Mat & data, const int & label)		// add data to training data
{
	cv::Mat tmp, data32F;
	data.convertTo(data32F, CV_32FC3);
	data32F*=1./255;
	if(usedColorspace!=-1)
		cv::cvtColor(data32F, tmp, usedColorspace);
	else
		tmp=data32F.clone();

	pictureChannels=tmp.channels();
	// tranform to data & label matrices
	transformPictureToData(tmp, label);
	DataSet=true;
}

bool SMOD::COLORSVM::trainSVM_withoutCrossValidation()
{
	if(!SVMTrained)
	{
		// scale data
		scaleData();
		// train
		SVMTrained=svmObject.train(DataRowSamples, LabelsRowSamples, cv::Mat(), cv::Mat(), svmParameters);
	}
	return SVMTrained;
}

bool SMOD::COLORSVM::trainSVM_withCrossValidation()		// perform training and cross-validation
{
	// train
	if(!SVMTrained)
	{
		// scale data
		scaleData();
		// train
		SVMTrained=svmObject.train_auto(DataRowSamples, LabelsRowSamples, cv::Mat(), cv::Mat(), svmParameters);
	}
	return SVMTrained;
}

bool SMOD::COLORSVM::saveModel(const std::string & modelPathAndName, const std::string & scaleValuesPathAndName)	// save
{
	if(SVMTrained)
	{
		saveScaleValues(scaleValuesPathAndName);
		svmObject.save(modelPathAndName.c_str());
		return true;
	}
	else 
		return false;
}

void SMOD::COLORSVM::loadModel(const std::string & modelPathAndName, const std::string & scaleValuesPathAndName)	// load from disk
{
	// load model
	std::cout << "Loading model " << modelPathAndName << "\n";
    svmObject.load(modelPathAndName.c_str());

	// load scale values
	std::cout << "Loading scale values " << scaleValuesPathAndName << "\n";
   // loadScaleValues(scaleValuesPathAndName);
    loadScaleValuesXML(scaleValuesPathAndName);
	scaleValuesComputed=true;

	SVMTrained=true;
	parametersSet=true;
}

void SMOD::COLORSVM::segmentPicture(const cv::Mat & in, cv::Mat & out)		// perform color segmentation
{
	// convert to colorspace
	cv::Mat in32F;
	in.convertTo(in32F, CV_32FC3);
	in32F*=1./255;
	cv::Mat inColorspace;
	if(usedColorspace!=-1)
	{
		cv::cvtColor(in32F, inColorspace, usedColorspace);
		pictureChannels=inColorspace.channels();
	}
	else
		inColorspace=in.clone();

	// treat
	DataSet=false;		// force new init of data matrix
	transformPictureToData(inColorspace, -1);
	// scale data
	scaleData();
	// predict
	cv::Mat predictedLabelsRowSamples;
	svmObject.predict(DataRowSamples, predictedLabelsRowSamples);

	// convert results to mask
	transformDataToMask(predictedLabelsRowSamples, in.size(), out);
}

void SMOD::COLORSVM::transformPictureToData(const cv::Mat & pictureF, const int & label)	// input should be 32F
{
	static cv::Mat ignoredColorMat;
	// if first initialisation
	if(!DataSet)
	{
		cv::Mat tmp=cv::Mat(1,1,CV_32FC3);
		tmp.ptr<float>(0)[tmp.channels()*0+0]=1.0;
		tmp.ptr<float>(0)[tmp.channels()*0+1]=1.0;
		tmp.ptr<float>(0)[tmp.channels()*0+2]=1.0;
		if(usedColorspace!=-1)
			cv::cvtColor(tmp, ignoredColorMat, usedColorspace);
		else
			ignoredColorMat=tmp.clone();
		
		DataRowSamples=cv::Mat(0, tmp.channels(), CV_32FC1);
		LabelsRowSamples=cv::Mat(0, 1, CV_32FC1);
		DataSet=true;
	}

	int ignoredCounter=0;
	bool forTraining=label>=0;


	// scan every pixel
	for(int x=0; x<pictureF.cols; x++)
	{
		for(int y=0; y<pictureF.rows; y++)
		{
			// ignore white pixels in training data
			/*for(int a=0; a<3; a++){
				//std::cout << pictureF.ptr<float>(y)[3*x+a] << "/" << ignoredColorMat.ptr<float>(0)[3*0+a] << " ";
				std::cout << (int)(pictureF.ptr<float>(y)[3*x+a]==ignoredColorMat.ptr<float>(0)[3*0+a]) << " ";
			}std::cout << std::endl;*/
			int ignoreThis=0;
			for(int i=0; i<pictureF.channels(); i++)
			{
				if(pictureF.ptr<float>(y)[pictureF.channels()*x+i]==ignoredColorMat.ptr<float>(0)[pictureF.channels()*0+i])
				{
					ignoreThis++;
				}
			}
			if(forTraining && ignoreThis==pictureF.channels())
			{
				ignoredCounter++;
			}
			else
			{
				// transform to data
				cv::Mat tmpData=cv::Mat(1, 3, CV_32FC1);
				for(int c=0; c<pictureF.channels(); c++)
					tmpData.at<float>(0, c)=pictureF.ptr<float>(y)[pictureF.channels()*x+c];			// at<type>(row, col); ptr<type>(row)[nchannels*col+channel]
				DataRowSamples.push_back(tmpData);
				// transform to label ( training only )
				if(forTraining)
				{
					cv::Mat tmpLabel=cv::Mat(1, 1, CV_32FC1);
					tmpLabel.at<float>(0,0)=(float)label;
					LabelsRowSamples.push_back(tmpLabel);
				}
			}
		}
	}

	if(forTraining)
	{
		std::cout << "Feature matrix is " << DataRowSamples.size() << ", and labels matrix is " << LabelsRowSamples.size() << "\n";
		std::cout << "-> " << ignoredCounter << " pixels ignored (white) for the last class\n";
	}
}

void SMOD::COLORSVM::scaleData()
{
	// compute scale values
	if(!scaleValuesComputed)
	{
		for(int i=0; i<pictureChannels; i++)
		{
			cv::Mat tmp=DataRowSamples.col(i);
			cv::Point2d scaleValues;
			cv::minMaxLoc(tmp, &scaleValues.x, &scaleValues.y, NULL, NULL);
			scaleValuesPerChannel.push_back(scaleValues);
		}
		scaleValuesComputed=true;
	}
	// perform feature scaling
	for(int f=0; f<DataRowSamples.cols; f++)
	{
		for(int n=0; n<DataRowSamples.rows; n++)
			DataRowSamples.at<float>(n, f)=(DataRowSamples.at<float>(n, f)-scaleValuesPerChannel[f].x)/(scaleValuesPerChannel[f].y-scaleValuesPerChannel[f].x);
	}
}

void SMOD::COLORSVM::transformDataToMask(const cv::Mat & labelMatrix, const cv::Size & maskSize, cv::Mat & mask)
{
	mask=cv::Mat(maskSize, CV_8UC1);
	int counter=0;
	for(int x=0; x<mask.cols; x++)
	{
		for(int y=0; y<mask.rows; y++)
		{
			mask.at<uchar>(y, x)=(int)labelMatrix.at<float>(counter, 0);
			counter++;
		}
	}
}


void SMOD::COLORSVM::saveScaleValues(const std::string & scaleValuesPathAndName)
{
    /*
	// save to disk
    std::ofstream output;
    output.open(scaleValuesPathAndName, std::ofstream::out);
	if(!output.is_open())
	{
		std::cout << "Failed to save scale values to disk\n";
	}
	for(int i=0; i<scaleValuesPerChannel.size(); i++)
	{
		std::cout << "Writing scale value (min feature " << i << ") : " << scaleValuesPerChannel[i].x << std::endl; 
		std::cout << "Writing scale value (max feature " << i << ") : " << scaleValuesPerChannel[i].y << std::endl;
		output << scaleValuesPerChannel[i].x << " ";
		output << scaleValuesPerChannel[i].y << "\n";
	}
    */
}

void SMOD::COLORSVM::loadScaleValues(const std::string & scaleValuesPathAndName)
{  
    /*// load scale values
	std::ifstream input;
	input.open(scaleValuesPathAndName, std::ifstream::in);
	if(!input.is_open())
	{
		std::cout << "Error opening scale values file. Exiting\n";
		exit(-1);
	}
	int parity=2;
	cv::Point2d tmpPt;
	while(!input.eof())
	{
		if(parity%2==0)
			input >> tmpPt.x;
		else
		{
			input >> tmpPt.y;
			scaleValuesPerChannel.push_back(tmpPt);
			std::cout << "Scale values " << tmpPt << " loaded\n";
		}
		parity++;
    }*/
}

bool SMOD::COLORSVM::loadScaleValuesXML(const std::string & scaleValuesPathAndName)
{


    //
    std::cout<<"csvm scale values file = "<<scaleValuesPathAndName<<std::endl;

    //XML document
    pugi::xml_document doc;
    std::ifstream nameFile(scaleValuesPathAndName.c_str());
    pugi::xml_parse_result result = doc.load(nameFile);

    if(!result)
    {
        std::cout<<"Error opening scale values file"<<std::endl;
        return false;
    }

    std::string readingValue;
    cv::Point2d tmpVal;
    pugi::xml_node channel1 = doc.child("channel1");
    pugi::xml_node channel2 = doc.child("channel2");
    pugi::xml_node channel3 = doc.child("channel3");

    // for each channel load the min and max scale values
    readingValue=channel1.child_value("minValue");
    std::istringstream convertedValue1(readingValue);
    convertedValue1>>tmpVal.x;
    readingValue=channel1.child_value("maxValue");
    std::istringstream convertedValue2(readingValue);
    convertedValue2>>tmpVal.y;
    scaleValuesPerChannel.push_back(tmpVal);
    std::cout << "Scale values " << tmpVal << " loaded\n";

    readingValue=channel2.child_value("minValue");
    std::istringstream convertedValue3(readingValue);
    convertedValue3>>tmpVal.x;
    readingValue=channel2.child_value("maxValue");
    std::istringstream convertedValue4(readingValue);
    convertedValue4>>tmpVal.y;
    scaleValuesPerChannel.push_back(tmpVal);
    std::cout << "Scale values " << tmpVal << " loaded\n";

    readingValue=channel3.child_value("minValue");
    std::istringstream convertedValue5(readingValue);
    convertedValue5>>tmpVal.x;
    readingValue=channel3.child_value("maxValue");
    std::istringstream convertedValue6(readingValue);
    convertedValue6>>tmpVal.y;
    scaleValuesPerChannel.push_back(tmpVal);
    std::cout << "Scale values " << tmpVal << " loaded\n";

    return true;
}

void SMOD::COLORSVM::removeValueChannel(cv::Mat & io)
{
	std::vector<cv::Mat> tmp;
	cv::split(io, tmp);
	tmp.pop_back();
	io=cv::Mat();
	cv::merge(tmp, io);
}

