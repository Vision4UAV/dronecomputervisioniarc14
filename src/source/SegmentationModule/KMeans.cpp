#pragma once

#include "SegmentationModule/SegmentationModule.h"

/////////////////////////////////
// KMEANS
////////////////////////////////
void SMOD::KM::run(const cv::Mat & in, cv::Mat & out, const int & K, const cv::Size & processingSize)
{
	cv::Mat KMtmp=in.clone();
	// resize if necessary for saving computing time
	if(processingSize.area()>0)
		UTI::images::resize(KMtmp, processingSize);	

	// process the picture using kmeans - color
	cv::Mat KMvec, KMinput, KMresult;
	KMvec=KMtmp.reshape(1, KMtmp.size().area());
	KMvec.convertTo(KMinput, CV_32FC3, 1.0/255.0);	// conversion to floating point
	cv::kmeans(KMinput, K, KMresult, cv::TermCriteria(cv::TermCriteria::EPS, 1, 0.01), 1, cv::KMEANS_PP_CENTERS, cv::noArray());
	KMresult=KMresult.reshape(1, KMtmp.size().height);
	KMresult.convertTo(out, CV_8UC1);
	if(processingSize.area()>0)
		UTI::images::resize(out,in.size());
	out+=cv::Mat::ones(out.size(), CV_8UC1);
	//UTI::exports::savePicture(KMtmp, "results\\KM", "result", "png", 0);
}

