#pragma once

#include "SegmentationModule/SegmentationModule.h"

void SMOD::EDGE::canny(const cv::Mat & grayInput, cv::Mat & edges, const cv::Point Thresholds)
{
	cv::Canny(grayInput, edges, Thresholds.x, Thresholds.y, 3, false);
}
