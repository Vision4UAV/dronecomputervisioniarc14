#pragma once

#include "SegmentationModule/SegmentationModule.h"

/////////////////////////////////
// GRABCUT
////////////////////////////////
void SMOD::grabcut::run(const cv::Mat & in, const cv::Mat & fgMask, const cv::Mat & probfgMask, const cv::Mat & probbgMask, const cv::Mat & bgMask, cv::Mat & out)
{
	cv::Mat fgModel, bgModel;
	// seed mask creation
	cv::Mat gcSeeds=cv::Mat::zeros(in.size(), CV_8UC1);
	cv::bitwise_or(gcSeeds, probbgMask*cv::GC_PR_BGD, gcSeeds, probbgMask);
	cv::bitwise_or(gcSeeds, bgMask*cv::GC_BGD, gcSeeds, bgMask);
	cv::bitwise_or(gcSeeds, probfgMask*cv::GC_PR_FGD, gcSeeds, probfgMask);
	cv::bitwise_or(gcSeeds, fgMask*cv::GC_FGD, gcSeeds, fgMask);
	// grabCut
	cv::grabCut(in, gcSeeds, cv::Rect(), bgModel, fgModel, 1, cv::GC_INIT_WITH_MASK);
	out=gcSeeds.clone();
}
