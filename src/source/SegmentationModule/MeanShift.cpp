#pragma once 

#include "SegmentationModule/SegmentationModule.h"

void SMOD::MS::run(const cv::Mat & in, cv::Mat & out, const int & spatialRes, const int & colorRes, const int & maxLevel, const cv::Size & processingSize)
{
	int sr, cr, lvl;
	if(spatialRes<=0)
		sr=10;
	else
		sr=spatialRes;
	if(colorRes<=0)
		cr=10;
	else
		cr=colorRes;
	if(maxLevel<1)
		lvl=1;
	else
		lvl=maxLevel;

	cv::Mat tmp=in.clone();
	cv::Mat result;
	// resize if necessary for saving computing time
	if(processingSize.area()>0)
		UTI::images::resize(tmp, processingSize);
	
	cv::pyrMeanShiftFiltering(tmp, result, sr, cr, lvl);
	if(processingSize.area()>0)
		UTI::images::resize(result, in.size());
	result.convertTo(out, CV_8UC3);
	//UTI::exports::savePicture(tmp, "results\\MS", "result", "png", 0);
}
