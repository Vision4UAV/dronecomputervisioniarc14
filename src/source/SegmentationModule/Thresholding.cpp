#pragma once

#include "SegmentationModule/SegmentationModule.h"

void SMOD::TH::run_std(const cv::Mat & in, cv::Mat & out, const cv::Vec3i thresholds, const cv::Size & processingSize)
{
	cv::Mat tmp=in.clone();
	if(processingSize.area()>0)
		UTI::images::resize(tmp, processingSize);
	std::vector<cv::Mat> chs=std::vector<cv::Mat>(3, cv::Mat());
	for(int i=0; i<3; i++)
	{
		cv::extractChannel(tmp, chs[i], i);
		cv::threshold(chs[i], chs[i], thresholds[i], 255, cv::THRESH_BINARY);
	}
	cv::merge(chs, out);
	if(processingSize.area()>0)
		UTI::images::resize(out, in.size());
	//UTI::exports::savePicture(out, "results\\TH", "result_std", "png", 0);
}

void SMOD::TH::run_std(const cv::Mat & in, cv::Mat & out, const cv::Vec3i thresholds, const cv::Size & processingSize, const int threshType=CV_THRESH_TOZERO)
{
	cv::Mat tmp=in.clone();
	if(processingSize.area()>0)
		UTI::images::resize(tmp, processingSize);
	std::vector<cv::Mat> chs=std::vector<cv::Mat>(3, cv::Mat());
	for(int i=0; i<3; i++)
	{
		cv::extractChannel(tmp, chs[i], i);
		cv::threshold(chs[i], chs[i], thresholds[i], 255, threshType);
	}
	cv::merge(chs, out);
	if(processingSize.area()>0)
		UTI::images::resize(out, in.size());
	//UTI::exports::savePicture(out, "results\\TH", "result_std", "png", 0);
}

void SMOD::TH::run_auto(const cv::Mat & in, cv::Mat & out_mean, const int & kernelSize, const cv::Size & processingSize)
{
	int ksize;
	if(kernelSize%2==0)
		ksize=kernelSize+1;
	else
		ksize=kernelSize;

	// pre-processing
	cv::Mat gray;
	cv::cvtColor(in, gray, CV_RGB2GRAY);
	if(gray.type()!=CV_8UC1)
		std::cout << "bad picture type (" << gray.type() << "\n";
	if(processingSize.area()>0)
		UTI::images::resize(gray, processingSize);

	// mean method
	cv::adaptiveThreshold(gray, out_mean, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, ksize, 0);
	if(processingSize.area()>0)
		UTI::images::resize(out_mean, in.size());
}

void SMOD::TH::run_autoG(const cv::Mat & in, cv::Mat & out_gaussian, const int & kernelSize, const cv::Size & processingSize)
{
	int ksize;
	if(kernelSize%2==0)
		ksize=kernelSize+1;
	else
		ksize=kernelSize;

	// pre-processing
	cv::Mat gray;
	cv::cvtColor(in, gray, CV_RGB2GRAY);
	if(gray.type()!=CV_8UC1)
		std::cout << "bad picture type (" << gray.type() << "\n";
	if(processingSize.area()>0)
		UTI::images::resize(gray, processingSize);

	// gaussian method
		cv::adaptiveThreshold(gray, out_gaussian, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, ksize, 0);
	if(processingSize.area()>0)
		UTI::images::resize(out_gaussian, in.size());
}
