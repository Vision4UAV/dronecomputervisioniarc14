#pragma once

#include "SegmentationModule/SegmentationModule.h"

/////////////////////////////////
// WATERSHED
////////////////////////////////
void SMOD::watershed::run(const cv::Mat & in, const std::vector<cv::Mat> & seedMasks, cv::Mat & out)
{
	// seed mask creation
	cv::Mat wsSeeds=cv::Mat::zeros(in.size(), CV_32FC1);
	for(int i=0; i<seedMasks.size(); i++)
	{
		cv::bitwise_or(wsSeeds, seedMasks[i]*(i+1), in, seedMasks[i]);
	}
	// watershed transform
	cv::Mat tmp;
	cv::watershed(in, wsSeeds);
	wsSeeds.convertTo(out, CV_8UC1);
}
