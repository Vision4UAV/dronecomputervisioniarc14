#pragma once
#include "CSAMPEDRO_HOUGH/Hough.h"

#include "CSAMPEDRO_HOUGH/precomp.hpp"	// todo find includes location
#include "CSAMPEDRO_HOUGH/_list.h"

typedef struct CvLinePolar
{
    float rho;
    float angle;
}
CvLinePolar;

const int STORAGE_SIZE = 1 << 12;

static void seqToMat(const CvSeq* seq, OutputArray _arr)
{
    if( seq && seq->total > 0 )
    {
        _arr.create(1, seq->total, seq->flags, -1, true);
        Mat arr = _arr.getMat();
        cvCvtSeqToArray(seq, arr.data);
    }
    else
        _arr.release();
}

#define hough_cmp_gt(l1,l2) (aux[l1] > aux[l2])
static CV_IMPLEMENT_QSORT_EX( icvHoughSortDescent32s, int, hough_cmp_gt, const int* )


CSH::OCV_Hough::OCV_Hough()
{
	// create look-up table 
	// for every degree in 0-PI
    trigoLookupTable=std::vector< std::vector<double> >(2, std::vector<double>());
	for(int i=0; i<179; i++)
	{
		double angle=(double)i*CV_PI/180.0;
		trigoLookupTable[0].push_back(cos(angle));
		trigoLookupTable[1].push_back(sin(angle));
	}
}

std::vector<cv::Point> CSH::OCV_Hough::getLines(const cv::Mat & InputArray, const double & rho_increment, const double & theta_min, const double & theta_max, const double & theta_increment, const int & num_peaks, const int & vote_threshold, const int neighborhood_size)
{
	return icvHoughLinesStandard(InputArray, rho_increment, theta_min, theta_max, theta_increment, num_peaks, vote_threshold, neighborhood_size);
}

/*
Here image is an input raster;
step is it's step; size characterizes it's ROI;
rho and theta are discretization steps (in pixels and radians correspondingly).
threshold is the minimum number of pixels in the feature for it
to be a candidate for line. lines is the output
array of (rho, theta) pairs. linesMax is the buffer size (number of pairs).
Functions return the actual number of found lines.
*/
// This is taken from OpenCV
// modifications are made for 
std::vector<cv::Point> CSH::OCV_Hough::icvHoughLinesStandard(const cv::Mat & input, const double & rho_increment, const int & theta_min, const int & theta_max, const int & theta_increment, const int & num_peaks, const int & vote_threshold, const int neighborhood_size )
{
	// convert input to CvMat
    CvMat img = input.clone();

    cv::AutoBuffer<int> _accum, _sort_buf;

    const uchar* image;
    int step, width, height;
    int numangle, numrho;
    int total = 0;
    int i, j;
    double scale;

    image = img.data.ptr;
    step = img.step;
    width = img.cols;
    height = img.rows;

    numrho = cvRound(((width + height) * 2 + 1) / rho_increment);
	numangle=cvRound((double)(theta_max-theta_min)/(double)theta_increment);
    _accum.allocate((numangle+2) * (numrho+2));
    _sort_buf.allocate(numangle * numrho);
    int *accum = _accum, *sort_buf = _sort_buf;
    memset( accum, 0, sizeof(accum[0]) * (numangle+2) * (numrho+2) );

    // stage 1. fill accumulator
    for( i = 0; i < height; i++ )
        for( j = 0; j < width; j++ )
        {
			int nn=0;
            if( image[i * step + j] != 0 )
				for(int n = theta_min; n < theta_max; n+=theta_increment )
                {
					int r = cvRound( j * trigoLookupTable[0][n] + i * trigoLookupTable[1][n] );
                    r += (numrho - 1) / 2;
                    accum[(++nn) * (numrho+2) + r+1]++;
                }
        }

    // stage 2. find local maximums
    for(int r = 0; r < numrho; r++ )
        for(int n = 0; n < numangle; n++ )
        {
            int base = (n+1) * (numrho+2) + r+1;
            if( accum[base] > vote_threshold &&
                accum[base] > accum[base - 1] && accum[base] >= accum[base + 1] &&
                accum[base] > accum[base - numrho - 2] && accum[base] >= accum[base + numrho + 2] )
                sort_buf[total++] = base;
        }

    // stage 3. sort the detected lines by accumulator value
    icvHoughSortDescent32s( sort_buf, total, accum );

    // stage 4. store the first min(total,linesMax) lines to the output buffer
	CvSeq* lines = 0;
	Ptr<CvMemStorage> lineStorage = cvCreateMemStorage(STORAGE_SIZE);
	lines = cvCreateSeq( CV_32FC2, sizeof(CvSeq), sizeof(float)*2, (CvMemStorage*)lineStorage );
    scale = 1./(numrho+2);
    for( i = 0; i < num_peaks; i++ )
    {
        CvLinePolar line;		
        int idx = sort_buf[i];
        int n = cvFloor(idx*scale) - 1;
        int r = idx - (n+1)*(numrho+2) - 1;
        line.rho = (r - (numrho - 1)*0.5f) * rho_increment;
        line.angle = theta_min+n * theta_increment;
        cvSeqPush( lines, &line );
    }

	std::vector<cv::Vec2f> detectedLines;	
	std::vector<cv::Point> returnedLines;
	seqToMat(lines, detectedLines);		
	int kend=cv::min((double)detectedLines.size(), (double)num_peaks);
	for(int k=0; k<kend; k++)
		returnedLines.push_back(cv::Point(detectedLines[k][1], detectedLines[k][0]));
	return returnedLines;	
}


CSH::OCV_Hough::~OCV_Hough()
{

}
