#include "IARC_2014_wrapper/Full_IARC_CV.h"



//////////////////////////////////////
// CAMERA UNDISTORTION
////////////////////////////////////
void cameraUndistortion::initialize(const cv::Size & pictureSize, const cv::Mat & camera_matrix, const cv::Mat & distorsion_coefficients)
{
	cameraPictureSize=cv::Size(pictureSize);
	cameraMatrix=camera_matrix.clone();
	distorsionCoefficients=distorsion_coefficients.clone();
	cameraCorrectionEngine.initUndistorsion(cameraMatrix, distorsionCoefficients, cameraPictureSize);
}

bool cameraUndistortion::run(const cv::Mat & in, cv::Mat & out)
{
	if(cameraMatrix.empty() || distorsionCoefficients.empty() || cameraPictureSize.area()==0)
	{
		std::cout << "!! Camera parameters have to be initialized before undistorting !!\n";
		return false;
	}
	cameraCorrectionEngine.performUndistorion(in, out);	
	return true;
}


//////////////////////////////////////
// COLOR SVM SEGMENTATION
////////////////////////////////////
void ColorSegmentationCSVM::initialize(const std::string & model_file, const std::string & scaleValues_file, const cv::Size & workingSize, const int & svm_type, const int & colorspace)
{
	wsize=cv::Size(workingSize);
	csvm.manualConstructor();
	csvm.setSVMParameters(svm_type);
	csvm.setColorspace(colorspace);
	csvm.loadModel(model_file, scaleValues_file);
}

bool ColorSegmentationCSVM::run(const cv::Mat & in, cv::Mat & out)
{
	// resize to working size	
	cv::Size orgsize=in.size();
	cv::Mat rescaled_in, rescaled_result;
	cv::resize(in, rescaled_in, wsize);

	// label the pixels
	csvm.segmentPicture(rescaled_in, rescaled_result);
	if(cv::countNonZero(rescaled_result)==0)
		return false;
	cv::resize(rescaled_result, out, orgsize);	


	return true;
}


//////////////////////////////////////
// VJ
////////////////////////////////////
bool BotDetectionVJ::initialize(const std::string & modelPathAndName)
{
	cv::Size minSize=cv::Size(50,50), maxSize=cv::Size(200, 200);
	cv::Point xThresholdsRASW=cv::Point(10,13);
	cv::Point yThresholdsRASW=cv::Point(10,13);
	double scaleFactor=1.20;
	int minNeighbors=3;
	botDetector=DET::cascadeDetector(minSize, maxSize, false, scaleFactor, minNeighbors, xThresholdsRASW, yThresholdsRASW);
	if(!botDetector.loadModel(modelPathAndName))
	{
		std::cout << "Failed loading detection model " << modelPathAndName << ".\n";
		return false;
	}
	else
	{
		std::cout << "Using detection model " << modelPathAndName << " with default parameters.\n";
		return true;
	}
}

bool BotDetectionVJ::initialize(const std::string & modelPathAndName, const cv::Size & minDetectionRectangleSize, const cv::Size & maxDetectionRectangleSize, const cv::Size & xThresholdsRASW, const cv::Size & yThresholdsRASW, const double & scaleFactor, const int & minNeighbors)
{
	botDetector=DET::cascadeDetector(minDetectionRectangleSize, maxDetectionRectangleSize, false, scaleFactor, minNeighbors, xThresholdsRASW, yThresholdsRASW);
	if(!botDetector.loadModel(modelPathAndName))
	{
		std::cout << "Failed loading detection model " << modelPathAndName << ".\n";
		return false;
	}
	else
	{
		std::cout << "Using detection model " << modelPathAndName << " with custom parameters.\n";
		return true;
	}
}

bool BotDetectionVJ::run(const cv::Mat & in, std::vector<cv::Rect> & out)
{
		botDetector.detect(in, out);
        //std::cout<<"Jeff's output"<<out[0]<<std::endl;
		if(out.empty())
			return false;
		else 
			return true;
}


//////////////////////////////////////
// HOGSVM
////////////////////////////////////
/*
bool BotDetectionHOGSVM::initialize(const std::string & modelPathAndName)
{
	return botDetector.loadModel(modelPathAndName);
}

bool BotDetectionHOGSVM::run(const cv::Mat & in, std::vector<cv::Rect> & out)
{
	return botDetector.detect(in, out);
}
*/


//////////////////////////////////////
// CamShift
////////////////////////////////////
void BotTrackingCamshift::initialize(const int & maxTrackers)
{
	for(int i=0; i<maxTrackers; i++)		// we will track max n bots per pictures
		botTracker.push_back(TRK::CamShift());
	trackerInitialized=false;
	successiveFramesTracked=0;
}

bool BotTrackingCamshift::computeReference(const cv::Mat & image, const cv::Mat & colorMask, const std::vector<cv::Rect> & in)
{
	if(in.empty())
	{
		std::cout << "Empty rectangle vector for CamShift\n";
		return false;
	}

	if(trackerInitialized)
	{
		std::cout << "!! Tracker already initialized !! \n";
		return false;
	}

	std::vector<bool> trackersInitialized;

	// reduce the ROI size for tracking initialization
	double resizeFactor=1.0;
	std::vector<cv::Rect> tmpin;
	for(int i=0; i<in.size(); i++)
		tmpin.push_back(cv::Rect(in[i].x+(1/resizeFactor-1)*in[i].width*(resizeFactor*0.5), in[i].y+(1/resizeFactor-1)*in[i].height*(resizeFactor*0.5), in[i].width*resizeFactor, in[i].height*resizeFactor));

	for(int i=0; i<min(in.size(), botTracker.size()); i++)
	{
		if(in[i].area()>10)		// if valid input (no empty rectangle)
		{
			//std::cout << "Tracker " << i << " rect init size is " << tmpin[i];
			trackersInitialized.push_back(botTracker[i].initializeTracking(image(tmpin[i]), colorMask(tmpin[i])));
			//std::cout << " - ready.\n";
		}
	}

	for(int i=0; i<trackersInitialized.size(); i++)
		if(trackersInitialized[i]==true)
		{
			trackerInitialized=true;
			return true;
		}
	
	return false;	// no tracker successfully initialized
}

bool BotTrackingCamshift::track(const cv::Mat & image, const std::vector<cv::Rect> & in, std::vector<cv::Rect> & out, std::vector<double> & botOrientations)
{
	int workingTrackers=0;

	if(in.empty())
	{
		std::cout << "Empty rectangle vector for CamShift\n";
		return false;
	}

	if(!trackerInitialized)
	{
		std::cout << "!! Run computeReference() before calling track() !! \n";
		return false;
	}

	// cleanup
	out.clear();
	botOrientations.clear();

	for(int i=0; i<min(in.size(), botTracker.size()); i++)
	{
		if(in[i].area()>10 && in[i].tl().x!=0 && in[i].tl().y!=0)		// if valid input (no empty rectangle, no TL)
		{
			// track
			cv::RotatedRect tmp_location;
			double tmp_orientation;
			//std::cout << "Tracker " << i << " rect track size is " << in[i];
			cv::Rect tmpRect=cv::Rect(in[i]);
			if(botTracker[i].performTracking(image, tmpRect, tmp_location, tmp_orientation))
			{
				out.push_back(tmp_location.boundingRect());
				//botOrientations.push_back(tmp_orientation);	// this is not working because bots do not have shape features easily discernable
				// alternative, based on the difference between bounding rectangles
				cv::Point2f tmpCenter=cv::Point2f(in[i].x+in[i].width*0.5, in[i].y+in[i].height*0.5);
				float deltaX=tmp_location.center.x-tmpCenter.x;
				float deltaY=tmp_location.center.y-tmpCenter.y;
				tmp_orientation=std::atan2(deltaY, deltaX)*180.0/CV_PI;
				//std::cout << tmpCenter << ", " << tmp_location.center << "\n";
				//std::cout << "deltaX = " << deltaX << ", deltaY = " << deltaY << "\n";
				botOrientations.push_back(tmp_orientation);
				//std::cout  << " - done.\n";
				workingTrackers++;
			}
			else	// insufficient search window for tracker i
			{
				out.push_back(cv::Rect());
				botOrientations.push_back(-1000);
				//std::cout  << " - failed.\n";
			}
		}
		else	// lost focus for tracker i
		{
			out.push_back(cv::Rect());
			botOrientations.push_back(-2000);
			std::cout  << "Tracker " << i << " lost focus after " << successiveFramesTracked << " frames.\n";
		}
	}
	if(workingTrackers==0)	// deactivate tracker 
	{
		std::cout  << "All trackers lost focus after " << successiveFramesTracked << " frames.\n";
		trackerInitialized=false;
		return false;
	}
	successiveFramesTracked++;
	return true;
}

void BotTrackingCamshift::stopTracking()
{
	trackerInitialized=false;
	successiveFramesTracked=0;
}


//////////////////////////////////////
// IFF
////////////////////////////////////
void BotIdentification::initialize(const cv::Scalar & whiteThresholds, const cv::Scalar & redThresholds, const cv::Scalar & greenThresholds,
	const double & HoughMin_voteInputHeight_ratio, const int & HoughMinAngle_degrees, 
		const int & HoughMaxAngle_degrees, const double & HoughVerticalROI_extensionFactor,
		bool useColor, bool useHough)
{
	wth=whiteThresholds;
	rth=redThresholds;
	gth=greenThresholds;
	min_voteInputHeight_ratio=HoughMin_voteInputHeight_ratio;
	minAngle_degrees=HoughMinAngle_degrees;
	maxAngle_degrees=HoughMaxAngle_degrees;
	verticalROI_extensionFactor=HoughVerticalROI_extensionFactor;
	useColorClues=useColor;
	useHoughClues=useHough;
	IFFObj.initialize(wth, rth, gth, min_voteInputHeight_ratio, minAngle_degrees, maxAngle_degrees, verticalROI_extensionFactor);
}

void BotIdentification::run_RGB_hough(const cv::Mat & image, const cv::Mat & mask, const std::vector<cv::Rect> & detector_output, std::vector<int> & labels)
{
	std::string botColor;
	for(int i=0; i<detector_output.size(); i++)
	{
		if(UTI::images::checkROIValidity(image.size(), detector_output[i]) && detector_output[i].area()>10)
		{
			cv::Mat colorMask;
			cv::bitwise_and(image(detector_output[i]),image(detector_output[i]), colorMask, mask(detector_output[i]));
			IFFObj.run(colorMask, image, detector_output[i], botColor, useColorClues, useHoughClues);
			if(!botColor.compare("other"))
				labels.push_back(0);
			else if(!botColor.compare("red"))
				labels.push_back(2);
			else if(!botColor.compare("green"))
				labels.push_back(1);
			else if(!botColor.compare("white"))
				labels.push_back(-1);
		}
		else
			labels.push_back(0);
	}
}

void BotIdentification::run(const cv::Mat & image, const cv::Mat & mask, const std::vector<cv::Rect> & detector_output, std::vector<int> & labels)
{
	// color clues
	run_RGB_hough(image, mask, detector_output, labels);

	// geometric clues
	// TODO

	// cooperation/majority vote
	// TODO
}

//////////////////////////////////////
// Bot labelling (for continuity)
////////////////////////////////////
BotBaptizer::BotBaptizer()
{

}

void BotBaptizer::init(const int & maxAllowedPositionDrift_pixels, const bool & useTypeMatching, const int & maxSkippedFrames)
{
	maxAllowedDrift=maxAllowedPositionDrift_pixels;
	useColorMatching=useTypeMatching;
	currentID_counter=1;
	consecutiveNotTreatedFrames=0;
	max_consecutiveNotTreatedFrames=maxSkippedFrames;
}

void BotBaptizer::run(const std::vector<cv::Rect> & currentPositions, const std::vector<int> & currentTypes, std::vector<int> & currentLabels)
{
	//std::cout << "\n!old vs new sizes : " << previousPositions.size() << "/"<<  currentPositions.size() << "\n";
	//std::cout << "\n!old vs new types : " << previousTypes.size() << "/"<<  currentTypes.size() << "\n";
	
	if(previousPositions.empty() && !currentPositions.empty())		// first run or outdated data
	{
		previousPositions=std::vector<cv::Rect>(currentPositions);
		previousTypes=std::vector<int>(currentTypes);
		previousLabels.clear();
		for(int i=0; i<previousPositions.size(); i++)		// assign new IDs
			previousLabels.push_back(currentID_counter++);
		consecutiveNotTreatedFrames=0;
	}
	else if(currentPositions.empty())	// we are not getting any info
	{
		consecutiveNotTreatedFrames++;
		if(consecutiveNotTreatedFrames>=max_consecutiveNotTreatedFrames)		// if we didnt get info for some time
		{
			previousPositions.clear();		// reinitialize the labelling process
			previousTypes.clear();
			previousLabels.clear();
			consecutiveNotTreatedFrames=0;
		}
	}
	else if(!previousPositions.empty() && !currentPositions.empty())	// we are able to work
	{
		consecutiveNotTreatedFrames=0;
	
		std::vector<bool> usedOld;		// don't use a position twice
		usedOld=std::vector<bool>(previousPositions.size(), false);

		for(int nyew=0; nyew<currentPositions.size(); nyew++)
		{
			//std::cout << "nyew = " << nyew <<" -- ";
			bool closeEnough=false;
			for(int old=0; old<previousPositions.size(); old++)
			{
				//std::cout << "old = " << old <<" -- ";
				if(!usedOld[old] && !closeEnough)		// if this old position was not consumed, and the bot's position wasn't updated
				{
					//std::cout << "closeEnough";
					closeEnough=checkPositionDrift(previousPositions[old], currentPositions[nyew]);		// check if close enough to be considered the same
					//std::cout << "done\n";
					if(closeEnough)		// if we can match a new position to an old, may be same ID
					{
						//std::cout << "!closeEnough";
						if(useColorMatching)	// check if it is the same color
						{
							//std::cout << "!Color";
							if(currentTypes[nyew]==previousTypes[old])		
								currentLabels.push_back(previousLabels[old]);	// yes = same ID
							else
								currentLabels.push_back(currentID_counter++);	// no = new ID
							//std::cout << " ok\n";
						}
						else
							currentLabels.push_back(previousLabels[old]);
						//std::cout << " done\n";

						usedOld[old]=true;		// remove this position so as not to assign the same ID twice
					}
				}
			}
			if(!closeEnough)		// if no old position was close enough, new ID.
			{
				currentLabels.push_back(currentID_counter++);
			}
		}

		// update the old positions with the new ones, which will become outdated
		previousPositions=std::vector<cv::Rect>(currentPositions);
		previousTypes=std::vector<int>(currentTypes);
		previousLabels=std::vector<int>(currentLabels);

	}

	/*std::cout << "labels :\n";
	for(int i=0; i<currentLabels.size(); i++)
	{
		std::cout << "current : " << currentLabels[i] << "\n";
	}
	for(int i=0; i<previousLabels.size(); i++)
	{
		std::cout << "previous : " << previousLabels[i] << "\n";
	}*/
	
}

bool BotBaptizer::checkPositionDrift(const cv::Rect & oldPosition, const cv::Rect & newPosition)
{
	//std::cout << "\nUsing rectangles : \nold :" << oldPosition << ", new :" << newPosition;
	cv::Point2i oldCenter=cv::Point2i(oldPosition.x+oldPosition.width/2, oldPosition.y+oldPosition.height/2);
	cv::Point2i newCenter=cv::Point2i(newPosition.x+newPosition.width/2, newPosition.y+newPosition.height/2);
	int drift=UTI::math::euclDist(oldCenter, newCenter);
	//std::cout << "\n";
	if(drift<=maxAllowedDrift)
		return true;
	else
		return false;
}


//////////////////////////////////////
// Grid segmentation - intersections and keylines
////////////////////////////////////
// constructors
GridLineFinder::GridLineFinder(const cv::Size workingWindowSize, const cv::Vec3i BGR_refinementThresholdsForBG, const double maskMaxWhiteRatio, 
	const cv::Size DT_cellCountXY, const int RANSCAC_MAXIMUM_ITERATIONS, const int RANSAC_MAXIMUM_INLIER_DISTANCE, 
	const int RANSAC_MINIMUM_INLIER_COUNT,const int Y_channel_treshold)
{
	wsize=workingWindowSize;
	BGR_refinementThresholds=BGR_refinementThresholdsForBG;
	refinement_whitePixelOccupancyRatio=maskMaxWhiteRatio;
	DT_cellCount=DT_cellCountXY;
	RANSAC_ITER=RANSCAC_MAXIMUM_ITERATIONS;
	RANSAC_MAX_INLIER_DISTANCE=RANSAC_MAXIMUM_INLIER_DISTANCE;
	RANSAC_MIN_INLIER_COUNT=RANSAC_MINIMUM_INLIER_COUNT;
    y_channel_thresh=Y_channel_treshold;
}

// intersection functions - private
bool GridLineFinder::autothreshold(const cv::Mat & in, const cv::Size & workSize, cv::Mat & out, const int & erosionIter)
{
	/*cv::Mat preAutoG;
	cv::cvtColor(in, preAutoG, CV_RGB2YCrCb);
	cv::Mat autoGPic;
	SMOD::TH::run_autoG(preAutoG, autoGPic, 49, workSize);
	for(int i=0; i<erosionIter; i++)
		cv::erode(autoGPic, autoGPic, cv::Mat());

	// "unblur" the result
	cv::threshold(autoGPic, autoGPic, 0, 255, cv::THRESH_BINARY);

	if(autoGPic.empty())
		return false;
	else
	{
		out=autoGPic.clone();
		return true;
	}*/

	// IARC 14 : use std threshold because adaptive fails (too uniform BG/FG)
	cv::Mat converted;
	cv::Mat oneChannel;
	cv::Mat treated;
    //converted=in.clone();
	cv::cvtColor(in, converted, CV_BGR2YCrCb);
    cv::extractChannel(converted, oneChannel, 0);
    cv::threshold(oneChannel, treated, y_channel_thresh, 255, CV_THRESH_BINARY);

#ifdef DEBUG_CV
    cv::imshow("thresh", treated);
    cv::waitKey(1);
#endif

	if(oneChannel.empty())
		return false;
	else
	{
		out=treated.clone();
		return true;
	}
}

bool GridLineFinder::floodFillGridMask(const cv::Mat & picture, const cv::Mat &autothresholdSeg, cv::Mat & out)
{
	// inits
	bool foundSeeds=false;
	int bandHeight=10;	// height of the bottom in pixels
	float cells=10;
	int bandWidth=std::floor((float)picture.size().width/cells);
	float whiteRatioPatch=0.1;
	float whiteRatioGrid=0.04;
	float whiteRatioMax=0.25;
	float totalROIPix=bandHeight*bandWidth;
	int currBottom=autothresholdSeg.size().height-1;
	cv::Mat bottomPic, DTresult;
	cv::Rect ROI;
	int maxPos[2];
	std::vector<cv::Point> seeds=std::vector<cv::Point>(cells, cv::Point());

	// look for seeds
	while(!foundSeeds && currBottom-bandHeight>0)
	{
		for(int i=0; i<cells; i++)
		{
			ROI=cv::Rect(i*(bandWidth-1), currBottom-bandHeight, bandWidth, bandHeight);
			bottomPic=autothresholdSeg(ROI).clone();
			//std::cout << (float)cv::countNonZero(bottomPic)/totalROIPix << "/" << whiteRatioPatch;
			float pixRatio=cv::countNonZero(bottomPic)/totalROIPix;
			if(pixRatio>whiteRatioPatch)
			{
				DTresult=cv::Mat(bottomPic.size(), CV_32FC1);
				cv::distanceTransform(bottomPic, DTresult, CV_DIST_C, 3);
				cv::minMaxIdx(DTresult, NULL, NULL, NULL, maxPos, bottomPic);
				seeds[i]=cv::Point(maxPos[1]+ROI.x, maxPos[0]+ROI.y);	// replace in the basis of the picture
				foundSeeds=true;
				//std::cout << " selected. Seed is : " << seeds[i];
			}
			//std::cout << "\n";
		}
		currBottom-=bandHeight;
	}
	if(foundSeeds=false)
	{
		//std::cout << "Failed to find a seed in the picture\n";
		return false;
	}

	// compute the edge picture
	cv::Mat ffillResult;
	cv::Mat tmpcanny;
	cv::Mat segPicColor;
	cv::bitwise_and(picture, picture, segPicColor, autothresholdSeg);
	canny(segPicColor, tmpcanny);
	// blur it to fill holes in the edges
	cv::blur(tmpcanny, tmpcanny, cv::Size(7,5));
	// perform the floodfill
	foundSeeds=false;
	for(int i=0; i<cells; i++)
	{
		if(foundSeeds)
			break;
		else if(seeds[i]!=cv::Point())
		{
			ffillResult=cv::Mat::zeros(autothresholdSeg.size().height+2,autothresholdSeg.size().width+2, CV_8UC1);
			cv::floodFill(tmpcanny, ffillResult, seeds[i], cv::Scalar(150), NULL, cv::Scalar(0), cv::Scalar(0), 4 + (255 << 8) + cv::FLOODFILL_MASK_ONLY);
			//std::cout << (float)cv::countNonZero(ffillResult)/ffillResult.size().area() << "/" << whiteRatioGrid;
			float pixRatio=(float)cv::countNonZero(ffillResult)/ffillResult.size().area();
			if(pixRatio>whiteRatioGrid && pixRatio<whiteRatioMax)
			{
				foundSeeds=true;
			}
		}
	}
	if(foundSeeds=false)
	{
		//std::cout << "Failed to floodfill the picture\n";
		return false;
	}
	else
	{
		out=ffillResult(cv::Rect(1, 1, autothresholdSeg.size().width,autothresholdSeg.size().height)).clone();
		// TODO add a verification if the floodfill performed well ?
		return true;
	}	
}

bool GridLineFinder::canny(const cv::Mat & in, cv::Mat & out)
{
	// blur the picture
	cv::Mat tmp1;
	cv::blur(in, tmp1, cv::Size(3, 3));

	// canny
	cv::Canny(tmp1, out, 200, 100, 3, true);

	return 0;
}

bool GridLineFinder::refineAndValidateColorMask(cv::Mat & io)
{

	// use color thresholding to remove eventual overflows of the floodfill due to edge holes
	cv::Vec3i thresholds=cv::Vec3i(80, 100, 150);
	SMOD::TH::run_std(io, io, BGR_refinementThresholds, io.size(), CV_THRESH_TOZERO);

	// check the ratio of non-black pixels; if too high, reject
	cv::Mat gray, graysingle;
	io.convertTo(gray, CV_BGR2GRAY);
	cv::extractChannel(gray, graysingle, 0);
	float ratio=(float)cv::countNonZero(graysingle)/graysingle.size().area();
	if(ratio>refinement_whitePixelOccupancyRatio)
	{
		//std::cout << "Invalid color mask, too many non-white pixels\n";
		return false;
	}
	else
		return true;
}

// shared functions - private
bool GridLineFinder::DTSampling(const cv::Mat & BWmask, std::vector<cv::Point> & localMaxima, const cv::Size & windowCount)
{
	cv::Mat BWmask2;
	if(BWmask.type()!=CV_8UC1)
		BWmask.convertTo(BWmask2, CV_8UC1);
	else
		BWmask2=BWmask.clone();
	// Distance transform
	cv::Mat DTout=cv::Mat(BWmask2.size(), CV_32FC1);
	cv::distanceTransform(BWmask2, DTout, CV_DIST_C, 3);
	DTout.convertTo(DTout, CV_16UC1);
	// local maxima
	int W=std::floor((float)DTout.size().width/windowCount.width);
	int H=std::floor((float)DTout.size().height/windowCount.height);
	double maxlocalval;
	cv::Mat ROI;
	cv::Rect ROIrect;
	cv::Point UL, LR, localMaximum;
	for(int i=0; i<windowCount.width; i++)
	{
		for(int j=0; j<windowCount.height; j++)
		{
			UL=cv::Point(i*W, j*H);
			LR=UL+cv::Point(W, H);
			if(i==windowCount.width-1 || j==windowCount.height-1)
			{
				UTI::images::checkCoordinateValidity(UL, DTout.size());
				UTI::images::checkCoordinateValidity(UL, DTout.size());
			}
			ROIrect=cv::Rect(UL, LR);
			cv::minMaxLoc(DTout(ROIrect), NULL, &maxlocalval, NULL, &localMaximum);
			
			if(maxlocalval>0)
			{
				//std::cout << "Max = " << maxlocalval << " at " << localMaximum+UL << "\n";
				localMaxima.push_back(localMaximum+UL);
			}
		}
	}

	if(localMaxima.size()<10)	// todo fix value experimentaly
		return false;
	else
		return true;
}

bool GridLineFinder::useRANSAC2pass(const cv::Mat & in, std::vector<cv::Point> & out, const bool isKeyline)
{
	out.clear();
	// use DT samping of local maxima
	cv::Mat BWMask, tmpa;
	cv::threshold(in, tmpa, 0, 255, cv::THRESH_BINARY);
	cv::extractChannel(tmpa, BWMask, 0);
	std::vector<cv::Point> localMaxima;
	if(!DTSampling(BWMask, localMaxima, DT_cellCount))
		return false;

	// perform RANSAC
	// 1st pass
	std::vector<struct line_model> lines, important_lines;
	if(isKeyline)
	{}//ransacobj.lines(localMaxima, lines, RANSAC_ITER, RANSAC_MAX_INLIER_DISTANCE, RANSAC_MIN_INLIER_COUNT_KEYLINE);
	else
		ransacobj.lines(localMaxima, lines, RANSAC_ITER, RANSAC_MAX_INLIER_DISTANCE, RANSAC_MIN_INLIER_COUNT);
	if(lines.empty())
		return false;
	
	// 2nd pass
	ransacobj.secondPass(lines, important_lines);
	if(important_lines.empty())
		return false;

	if(isKeyline)	// keylines
	{
		// put the most important line into the output
		out.push_back(important_lines[0].pt1);
		out.push_back(important_lines[0].pt2);
	}
	else	// intersections
	{
		// Find line intersection by means of algebra
		std::vector<float> deltas;
		if(!computeLineIntersections(important_lines, out, deltas))
			return false;
	}

	return true;
}
		//////////// PUBLIC ////////////

bool GridLineFinder::run(const cv::Mat & undistortedFrame, std::vector<cv::Point> & out)
{
	out.clear();

	// Emphasize the lines using adaptative thresholding. 
	// IARC 2014 : failure of adaptive. As the BG is fairly uniform, use std thresholding
    cv::Mat threshpic;
	if(!autothreshold(undistortedFrame, wsize, threshpic, 0))	
        return false;

	// now let's get the white pixels on the bottom of the picture as seeds of the grid
	// IARC 2014 : useless as segmentation is std threshold-based
	cv::Mat gridMask=threshpic.clone();
	/*cv::Mat gridMask;
	if(!floodFillGridMask(undistortedFrame, threshpic, gridMask))
		return false;*/

	// check the colored mask. use color segmentation to refine it, then reject it if it has too many pixels (which would hence include BG)
	// IARC 2014 : useless as segmentation is std threshold-based
	cv::Mat ColorMask=gridMask.clone();
	/*cv::Mat ColorMask;
	UTI::images::createColoredMask(undistortedFrame, gridMask, ColorMask);	
	if(!refineAndValidateColorMask(ColorMask))
        return false;*/

    // Use 2 pass RANSAC for getting the lines and their intersection
	if(!useRANSAC2pass(ColorMask, out, false))
        return false;

	// if none of the above failed
	return true;
}

void GridLineFinder::intersectionClustering(const std::vector<cv::Point> & in, std::vector<cv::Point> & out, const int & clusteringRadius)
{
	out.clear();
	// start with a point of the vector and agregate as much points as possible
	// do this for all points

	std::vector<bool> pointConsumed=std::vector<bool>(in.size(), false);	// consumed points belong to a cluster

	for(int i=0; i<in.size(); i++)
	{
		if(pointConsumed[i])
		{}
		else
		{
			pointConsumed[i]=true;		// this point has been used
			cv::Point2f currentCluster=cv::Point(in[i]);
			int clusterMembers=1;
			for(int j=i+1; j<in.size(); j++)
			{
				if(!pointConsumed[j])	// only compare the other points
				{
					double dist=UTI::math::euclDist(currentCluster, in[j]);
					if(dist<=clusteringRadius)		// if close enough to the current root point, agregate
					{								// and considered the point as consumed
						currentCluster.x+=in[j].x;
						currentCluster.x/=(++clusterMembers);
						currentCluster.y+=in[j].y;
						currentCluster.y/=(clusterMembers);
						pointConsumed[j]=true;
					}
				}
			}
			out.push_back(currentCluster);	// add cluster to output
		}
	}
}



//////////////////////////////////////
// Grid Tracking - Kalman-based - bad results
////////////////////////////////////
void GridIntersectionTracker::initialize(const int & maxTrackers, const double & Vx_weight, const double & Vy_weight, const double & Ax_weight, const double & Ay_weight)
{
	for(int i=0; i<maxTrackers; i++)
		gridTracker.push_back(TRK::Kalman());

	for(int i=0; i<maxTrackers; i++)
	{
		gridTracker[i].setTransitionMatrixWeights(Vx_weight, Vy_weight, Ax_weight, Ay_weight);
		gridTracker[i].init(6, 2, 0);
	}
}

bool GridIntersectionTracker::track(const std::vector<cv::Point> & gridIntersections, std::vector<cv::Point> & predictedIntersections)
{
	predictedIntersections.clear();
	for(int i=0; i<min(gridIntersections.size(), gridTracker.size()); i++)
	{
		cv::Point tmp;
		gridTracker[i].predict(gridIntersections[i], tmp);
		predictedIntersections.push_back(tmp);
	}
	return true;
}

bool GridIntersectionTracker::updateTracker(const std::vector<cv::Point> & gridIntersections, std::vector<cv::Point> & estimatedIntersections)
{
	estimatedIntersections.clear();
	for(int i=0; i<min(gridIntersections.size(), gridTracker.size()); i++)
	{
		cv::Point tmp;
		gridTracker[i].correct(gridIntersections[i], tmp);
		estimatedIntersections.push_back(tmp);
	}
	return true;
}


