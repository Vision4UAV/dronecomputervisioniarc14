// IFF.cpp : d�finit le point d'entr�e pour l'application console.
// Identify wether a bot is a target or a threat (pole)

#include "IFF/IFF.h"

void IFF::initialize(const cv::Scalar & botWhiteThresholds, const cv::Scalar & botRedThresholds, 
	const cv::Scalar & botGreenThresholds, const double & HoughMin_voteInputHeight_ratio, const int & HoughMinAngle_degrees, 
	const int & HoughMaxAngle_degrees, const double & HoughVerticalROI_extensionFactor)
{
	whiteThresholds=cv::Scalar(botWhiteThresholds);
	redThresholds=cv::Scalar(botRedThresholds);
	greenThresholds=cv::Scalar(botGreenThresholds);
	min_voteInputHeight_ratio=HoughMin_voteInputHeight_ratio;
	minAngle_degrees=HoughMinAngle_degrees;
	maxAngle_degrees=HoughMaxAngle_degrees;
	verticalROI_extensionFactor=HoughVerticalROI_extensionFactor;
    //HoughObj=CSH::Hough::Hough(false);
}

cv::Scalar IFF::useColorInfoRGB(const cv::Mat & input, std::string & botColor)
{
	// preparation
	std::vector<cv::Mat> channels;
	cv::split(input, channels);
	cv::Mat blueHist, greenHist, redHist;

	// histogram computation
	UTI::images::computeHistogram(channels[0], blueHist);
	UTI::images::computeHistogram(channels[1], greenHist);
	UTI::images::computeHistogram(channels[2], redHist);

	// find median value in each histogram 
	int tmp1, tmp2, tmp3;
	// 255th bin seems not to be representative...
	blueHist.at<float>(cv::Point(0,255))=0;
	greenHist.at<float>(cv::Point(0,255))=0;
	redHist.at<float>(cv::Point(0,255))=0;
	// ignore black pixels (background from the mask)
	blueHist.at<float>(cv::Point(0,0))=0;
	greenHist.at<float>(cv::Point(0,0))=0;
	redHist.at<float>(cv::Point(0,0))=0;
	cv::Point bluePeakPos, greenPeakPos, redPeakPos;
	UTI::images::computeHistogramMedian(blueHist, tmp1);
	UTI::images::computeHistogramMedian(greenHist, tmp2);
	UTI::images::computeHistogramMedian(redHist, tmp3);

	// compare
	if(tmp3<greenThresholds[0] && tmp2>greenThresholds[1] && tmp1>greenThresholds[2])		// green bot
	{botColor="green";}
	else if(tmp3>redThresholds[0] && tmp2<redThresholds[1] && tmp1<redThresholds[2])	// red bot
	{botColor="red";}
	else if(tmp3>whiteThresholds[0] && tmp2>whiteThresholds[1] && tmp1>whiteThresholds[2])	// white bot
	{botColor="white";}
	else	// not a bot according to color
	{botColor="other";}

	/*std::cout << "GTH = " << greenThresholds << "\n";
	std::cout << "RTH = " << redThresholds << "\n";
	std::cout << ">TH = " << whiteThresholds << "\n";*/

	
	// debug
	/*cv::Mat histMat;
	UTI::images::plotHistogram(blueHist, histMat, cv::Scalar(255,0,0), true);
	UTI::images::plotHistogram(greenHist, histMat, cv::Scalar(0,255,0), false);
	UTI::images::plotHistogram(redHist, histMat, cv::Scalar(0,0,255), false);
	cv::imshow("histograms", histMat);*/
	/*std::cout << "Peak values are :\n" << "R = " << tmp3 << "@" << redPeakPos 
		<< ", G = " << tmp2 << "@" << greenPeakPos 
		<< ", B = " << tmp1 << "@" << bluePeakPos << "\n";*/
	/*cv::Mat tmpimg;
	cv::resize(input, tmpimg, cv::Size(640, 480));
	cv::imshow("bot picture", tmpimg);*/
	//std::cout << "Bot color is hence " << botColor << "\n";
	//cv::waitKey(0);

	return cv::Scalar(tmp1, tmp2, tmp3);
}

int IFF::checkPole(const cv::Mat & input)
{
	// TODO : reduce input width so as to identify only vertical lines aligned on the bot


	// segment the input so it can be used
    cv::Mat blurredIn, cannyEdges, inputGray;
    cv::cvtColor(input, inputGray, CV_BGR2GRAY);
	// blur the picture
    cv::blur(inputGray, blurredIn, cv::Size(3, 3));
	// canny
	cv::Canny(blurredIn, cannyEdges, 200, 100, 3, true);

	// use hough transform - carlos version
	int neighborhoodSize=20;
	cv::Mat EH=HoughObj.EspacioHough_OPT(cannyEdges, minAngle_degrees, maxAngle_degrees, 1);
	std::vector<int> votes;
	std::vector<cv::Point> lines=HoughObj.MaximosHough(EH, 1, min_voteInputHeight_ratio*input.size().height/verticalROI_extensionFactor, neighborhoodSize, votes);
	if(lines.empty())
	{
		//std::cout << "no votes" << "/" << min_voteInputHeight_ratio*input.size().height << " votes obtained. IFF says no pole.\n";
		return 0;
	}
	else
	{
		//std::cout << votes[0] << "/" << min_voteInputHeight_ratio*input.size().height/verticalROI_extensionFactor << " votes obtained. IFF says !!pole!!.\n";
		return 1;
	}

}

void IFF::mergeDecisions(const std::string & botColor_color, const int & isPole, std::string & botColor)
{
	if(!botColor_color.compare("notrun"))		// color info not used
	{
		if(isPole==-1)		// pole info not used
			botColor="other";
		else	// use pole info
		{
			if(isPole==1)
				botColor="white";
			else
				botColor="other";
		}
	}
	else
	{
		if(isPole==-1)		// pole info not used
			botColor=std::string(botColor_color);
		else	// cooperate
		{
			if(isPole==1 && (!botColor_color.compare("other")))
				botColor="white";
			else if(isPole==1 && (!botColor_color.compare("white")))
				botColor="white";
			else if(isPole==1 && (!botColor_color.compare("red")))
				botColor="other";
			else if(isPole==1 && (!botColor_color.compare("green")))
				botColor="other";
			else if(isPole==0 && (!botColor_color.compare("other")))
				botColor="other";
			else if(isPole==0 && (!botColor_color.compare("white")))
				botColor="other";
			else if(isPole==0 && (!botColor_color.compare("red")))
				botColor="red";
			else if(isPole==0 && (!botColor_color.compare("green")))
				botColor="green";
			else
				botColor="other";
		}
	}
	//std::cout << "Bot color is " << botColor_color << ", pole decision is " << isPole << " -> decision is : " <<botColor <<"\n";
}

void IFF::run(const cv::Mat & segmentationROI, const cv::Mat & input, const cv::Rect & ROI, std::string & botColor, const bool useColor, const bool useHough)
{
	std::string botColor_color="notrun";
	int isPole=-1; // 0 isn't, +1 is, -1 unknown

	if(useColor && !segmentationROI.empty())
	{
		useColorInfoRGB(segmentationROI, botColor_color);
	}

	if(useHough && !input.empty() && ROI.area())
	{
		//std::cout << "using poles " << useHough << "\n";
		cv::Point topleft=ROI.tl();
		cv::Point bottomright=ROI.br();
		topleft.y=topleft.y-verticalROI_extensionFactor*ROI.height;
		if(UTI::images::checkCoordinateValidity(topleft, input.size()))
		{
			cv::Rect ROI_extended=cv::Rect(topleft, bottomright);
			isPole=checkPole(input(ROI_extended));
		}
	}

	mergeDecisions(botColor_color, isPole, botColor);
}




//void IFF::segmentBot(const cv::Mat & input, cv::Mat & output)
//{
//	// TODO
//}


//int IFF::useShapeInfo(const cv::Mat & input, const double & minCircularity, const double & minEccentricity)	// return if it is a bot or not. Give the thresholds for each criteria.
//{
//	////////////////
//	// first extract the biggest contour of the input. Should be the bot. 
//	std::vector<std::vector<cv::Point>> contours_init;
//	std::vector<cv::Point> contours;
//	cv::findContours(input.clone(), contours_init, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE, cv::Point(0,0));
//	const int MINBOTSIZE=25;
//	if(contours_init.size()<1)		
//		return 0;	// no contour found : error, hence unknown.
//	else if(contours_init.size()==1)
//		contours=std::vector<cv::Point>(contours_init[0]);
//	else
//	{
//		int maxContourIdx=0;
//		int maxContour=0;
//		int currSize=0;
//		for(int i=0; i<contours_init.size(); i++)
//		{
//			currSize=contours_init[i].size();
//			if(currSize>maxContourIdx)
//			{
//				maxContour=currSize;
//				maxContourIdx=i;
//			}
//		}
//		contours=std::vector<cv::Point>(contours_init[maxContourIdx]);
//	}
//	////////////////////
//
//	///////////////////
//	// Then compute the shape descriptors
//	// Circularity
//	double botArea=UTI::math::computeContourArea(contours);
//	double botPerimeter=UTI::math::computeContourPerimeter(contours);
//	double circularity=4*CV_PI*botArea/(botPerimeter*botPerimeter);
//
//	// Eccentricity
//	double eccentricity;
//	cv::RotatedRect boundingRect=cv::minAreaRect(contours);
//	float axis1Size=boundingRect.size.width;
//	float axis2Size=boundingRect.size.height;
//	if(axis1Size>axis2Size)
//		eccentricity=axis2Size/axis1Size;
//	else
//		eccentricity=axis1Size/axis2Size;
//	///////////////////
//
//	///////////////////
//	// Now take a decision
//	if(eccentricity>=minEccentricity && circularity>=minCircularity)
//		return 1;		// bot
//	else if(eccentricity>=minEccentricity)
//		return 0;		// one criteria valid, the other not. unknown
//	else if(circularity>=minCircularity)
//		return 0;
//	else				// one criteria valid, the other not. unknown
//		return -1;		// not a bot
//	///////////////////
//
//	// Debug
//	cv::imshow("input", input);
//	std::cout << "Circularity is " << circularity << "\n";
//	std::cout << "Eccentricity is " << eccentricity << "\n";
//	cv::waitKey(0);
//}


//void IFF::useColorInfoHSV(const cv::Mat & input, const cv::Scalar & minGreen, const cv::Scalar & maxGreen, const cv::Scalar & minRed, const cv::Scalar & maxRed, const cv::Scalar & minWhite, const cv::Scalar & maxWhite, std::string & botColor)
//{
//	UTI::timer timer;
//	timer.start();
//	// preparation
//	cv::Mat HSVimage;
//	cv::cvtColor(input, HSVimage, CV_BGR2HSV);
//
//	// Identification
//	cv::Mat resultG, resultR, resultW;
//	cv::inRange(input, minGreen, maxGreen, resultG);
//	cv::inRange(input, minRed, maxRed, resultR);
//	cv::inRange(input, minWhite, maxWhite, resultW);
//
//	int nonZG=cv::countNonZero(resultG);
//	int nonZR=cv::countNonZero(resultR);
//	int nonZW=cv::countNonZero(resultW);
//
//	/*std::cout << "Green count is " << nonZG << "\n";
//	std::cout << "Red count is " << nonZR << "\n";
//	std::cout << "White count is " << nonZW << "\n";*/
//
//	if(nonZG>nonZR && nonZG>nonZW)
//		botColor="green";
//	else if(nonZR>nonZG && nonZR>nonZW)
//		botColor="red";
//	else if(nonZW>nonZG && nonZW>nonZR)
//		botColor="white";
//	else
//		botColor="other";
//
//	std::cout << "IFF took ";
//	timer.showElapsed();
//	
//
//}

