// IARC_interface_reduced_test.cpp : d�finit le point d'entr�e pour l'application console.
//

#include "stdafx.h"


int main(int argc, char * argv[])
{
	///////////////////////////////////////////
	// PREPARATION
	//////////////////////////////////////////
	if(argc!=5)
	{
		std::cout << "Please give a cascade model, an svm model, svm scale values and a video as inputs.\n\n";
		exit(0);
	}

	std::cout << "Using cascade model :\n" << argv[1] 
	<< "\nand svm model :\n" << argv[2]
	<< "\nwith scale values :\n" << argv[3]
	<< "\non video :\n" << argv[4] << "\n";

	UTI::video videoInput=UTI::video("read");
	if(!videoInput.create(argv[4]))
	{
		std::cout << "Failed opening video file.\n";
		exit(-1);
	}

	UTI::video videoOutput=UTI::video("write");
	std::string outname="OUT.avi";
	if(!videoOutput.create(outname, videoInput.getFrameSize(), 15))
	{
		std::cout << "Failed creating output video file.\n";
	}

	// these values are in RGB (for drawing purposes)
	cv::Scalar unknownColor=cv::Scalar(0,255,255);
	cv::Scalar poleColor=cv::Scalar(255,255,255);
	cv::Scalar targetColor=cv::Scalar(77,234,60);

	// INITS
	IARC14_bot_node IA14B;
	IARC14_grid_node IA14G;

	// CAMERA
	cv::Mat cameraMatrix=cv::Mat::zeros(3,3, CV_64FC1);
	cameraMatrix.at<double>(0,0)=7.6987670872322428e+002; cameraMatrix.at<double>(0,2)=3.7550000000000000e+002;
	cameraMatrix.at<double>(1,1)=7.6987670872322428e+002; cameraMatrix.at<double>(1,2)=2.3950000000000000e+002;
	cameraMatrix.at<double>(2,2)=1.0;
	cv::Mat distorsionCoefficients=cv::Mat::zeros(5,1, CV_64FC1);
	distorsionCoefficients.at<double>(0,0)=-3.7406898890672224e-001; distorsionCoefficients.at<double>(1,0)=-1.4146168549641180e-001; distorsionCoefficients.at<double>(4,0)=6.3684725780972273e-001;
	IA14B.initializeCameraParameters(videoInput.getFrameSize(), cameraMatrix, distorsionCoefficients);
	IA14G.initializeCameraParameters(videoInput.getFrameSize(), cameraMatrix, distorsionCoefficients);

	// CSVM
	cv::Size csvmWorkSize=cv::Size(140,105);	// reduce the size of the frame for speeding up segmentation
	IA14B.initializeColorSVM_segmentation(argv[2], argv[3], csvmWorkSize, cv::SVM::LINEAR, CV_BGR2Lab);

	// BOT-RELATED
	cv::Size minBotSize=cv::Size(50, 50);	// don't look for smaller bots
	cv::Size maxBotSize=cv::Size(200,200);	// don't look for larger bots
	cv::Size xRASW=cv::Size(10,13);			// stages for slowing the run-time adaptive sliding window
	cv::Size yRASW=cv::Size(10,13);			// for Viola-Jones detector. before first stage it goes 
											// fast, then at first stage (xRASW.x) it slows to medium speed,
											// at second stage (xRasw.y) it is slow. xRASW controls x direction
											// (horizontal), and yRASW y direction (vertical)
	double scaleFactor=1.10;				// Viola-Jones scale factor for multiscale search
	int minNeighbors=3;						// The more neighbors the less FP, but you risk FN
	int successiveFramesToTrack=5;			// how many frames to track before reverting to detection
	int maxBotsToTrack=4;					// how many bots do we try to track at most ?
	IA14B.initializeDetectionModule(argv[1], minBotSize, maxBotSize, xRASW, yRASW, scaleFactor, minNeighbors, 1, 
		successiveFramesToTrack, maxBotsToTrack);
	double HoughMin_voteInputHeight_ratio=3.0;		// how many votes are needed for identifying a pole, expressed in ratio 
													// of the input height (roughly the bot's size)
	int HoughMinAngle_degrees=70;					// min/max angles in degree to search for lines (from the horizontal)
	int HoughMaxAngle_degrees=110;					//
	double HoughVerticalROI_extensionFactor=4;		// by which factor to increase the vertical dimension of the bot ROI
	cv::Scalar whiteThreshold=cv::Scalar(150, 150, 150);		// This is not used at the moment
	cv::Scalar redThreshold=cv::Scalar(60, 20, 20);				// a bot is red if : R>redThreshold[0], G<redThreshold[1], B<redThreshold[2]
	cv::Scalar greenThreshold=cv::Scalar(30, 30, 10);			// a bot is green if : R<greenThreshold[0], G>greenThreshold[1], B>greenThreshold[2]
	bool useColorClues=true;											// identify bots based on color ?
	bool useHoughClues=true;											// identify bots based on pole detection ?
	IA14B.initializeIdentificationModule(whiteThreshold, greenThreshold, redThreshold, HoughMin_voteInputHeight_ratio, 
		HoughMinAngle_degrees, HoughMaxAngle_degrees, HoughVerticalROI_extensionFactor, useColorClues, useHoughClues);
		
	int maxAllowedDrift=80;
	bool useTypeMatching=false;
	int maxSkippedFrames=10;
	IA14B.initializeLabellingModule(maxAllowedDrift, useTypeMatching, maxSkippedFrames);

	// GRID-RELATED
	cv::Size gridWorkSize=cv::Size(140,105);	// reduce the size of the frame for speeding up segmentation
	cv::Vec3i BGR_valuesForBG=cv::Vec3i(80, 100, 150);	// used for sorting grid from BG after segmentation 
	double maskWhiteRatio=0.15;					// used for sorting segmentation classes
	cv::Size DTCells=cv::Size(15,10);			// how many grids we want for finding local maxima of the distance transform
	int ransac_max_iterations=10000;			// do not change
	int ransac_max_inlier_distance=3;			// do not change
	int ransac_min_inliers=6;					// lower if you have too many FN, increase if you have too many FP
	int clusteringRadius=60;					// clustering radius for the intersections. 
	IA14G.initializeGridModule(gridWorkSize, BGR_valuesForBG, maskWhiteRatio, DTCells, ransac_max_iterations, 
		ransac_max_iterations, ransac_min_inliers, clusteringRadius);

	///////////////////////////////
	// RUNNING
	///////////////////////////////
	cv::Mat frame, undistortedFrame, result;
	std::string botColor;
	std::vector<cv::Rect> ROIs;
	std::vector<int> botTypes;
	std::vector<int> botLabels;
	std::vector<double> botOrientations;
	std::vector<cv::Point> gridIntersections;
	int trackingFrameCounter;
	int c=0, counter=0;
	char buf[10];
	UTI::timer timer;
	while(videoInput.nextFrame(frame) && c!=27)
	{
		// inits
		timer.start();
		int botCount=0, poleCount=0, unknownCount=0;

		// running - grid
		int errorCodeG=IA14G.run(frame, undistortedFrame, gridIntersections);

		// running - bots
		int errorCodeB=IA14B.run(frame, undistortedFrame, ROIs, botTypes, botLabels);
		//std::cout << "------------- FRAME TERMINATED : ERRORCODE " << errorCode << "\n";

		/*std::cout << "bot labels are : ";
		for(int i=0; i<botLabels.size(); i++)
			std::cout << botLabels[i] << ", ";
		std::cout << "\n";*/

		// outputs
		result=undistortedFrame.clone();
		// outputs - grid
		for(int i=0; i<gridIntersections.size(); i++)
		{
			cv::circle(result, gridIntersections[i], 7, cv::Scalar(255,0,127), -1);
			//cv::circle(result, gridIntersections[i], intersectionClusteringRadius, cv::Scalar(255,0,127), 1);
		}
		// outputs - bots
		for(int i=0; i<botLabels.size(); i++)
		{
			UTI::images::putText(result, ROIs[i].br(), itoa(botLabels[i], buf, 10));
		}
		for(int i=0; i<ROIs.size(); i++)
		{
			if(botLabels.empty())
			{
				cv::rectangle(result, ROIs[i], unknownColor, 1);
				unknownCount+=ROIs.size();
			}
			else
			{
				// increment botcount/polecount if a bot/pole was identified
				if(botTypes[i]<0)
				{
					poleCount++;
					cv::rectangle(result, ROIs[i], poleColor, 1);
				}
				else if(botTypes[i]>0)
				{
					botCount++;
					cv::rectangle(result, ROIs[i], targetColor, 1);
				}
				else
				{
					unknownCount++;
					cv::rectangle(result, ROIs[i], unknownColor, 1);
				}
			}
		}
		timer.showFPS(result, 10);
        std::string botText=itoa(botCount, buf, 10);
		botText+=" BOT(S)";
		UTI::images::putText(result, cv::Point(result.size().width*0.5, result.size().height/15), botText);
        std::string poleText=itoa(poleCount, buf, 10);
		poleText+=" POLE(S)";
		UTI::images::putText(result, cv::Point(result.size().width*0.75, result.size().height/15), poleText);
        std::string unknownText=itoa(unknownCount, buf, 10);
		unknownText+=" ???";
		UTI::images::putText(result, cv::Point(result.size().width*0.35, result.size().height/15), unknownText);

		videoOutput.nextFrame(result);
		cv::imshow("VJ test", result);

		c=cv::waitKey(1);
		while(c==32)
			c=cv::waitKey(0);
		counter++;
	}

	//////////////////////////////////////////
	// STOPPING
	/////////////////////////////////////////
	if(c==27)
		std::cout << "Process interrupted by the user. Total frame count was " << counter << "\n";
	else
		std::cout << "No more frames. Total frame count was " << counter << "\n";
	videoInput.release();
	videoOutput.release();

	return 0;
}

