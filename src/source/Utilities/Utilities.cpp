#pragma warning(disable : 4006)

#include "Utilities/Utilities.h"

void UTI::exports::savePicture(const cv::Mat & picture, const std::string repName, const std::string & name, const std::string & ext, const int & counter)
{
	char picCountChar[255];
	std::sprintf(picCountChar, "%d", counter);
	//std::cout << "counter = " << picCountChar << std::endl;
	std::string outname=repName+"\\"+name+"_"+picCountChar+"."+ext;
	//std::cout << "saving " << outname << std::endl;
	cv::imwrite(outname, picture);
}


////////////////////////////
// IMAGES
////////////////////////////
void UTI::images::resize(cv::Mat & io, const cv::Size & processingSize)
{
	cv::Mat tmp;
	cv::resize(io, tmp, processingSize);
	io=tmp.clone();
}

void UTI::images::separatePartitions(const cv::Mat & in, const int & num, std::vector<cv::Mat> & out)
{
	cv::Mat tmp, tmp2;
	for(int i=1; i<num+1; i++)
	{
		tmp=cv::Mat::zeros(in.size(), CV_8UC1);
		tmp2=cv::Mat::zeros(in.size(), CV_8UC1);
		// keep only the current partition i
		cv::threshold(in, tmp, i, 1, cv::THRESH_TOZERO_INV);	// zero all values above the current partition i; keep all values under the current partition i.
		cv::threshold(tmp, tmp2, i-1, 1, cv::THRESH_TOZERO);	// zero all values under the current partition i; keep all values above unchanged (all above are zeros due to the previous line)
		out.push_back(tmp2.clone());
	}
}

void UTI::images::morphologyCleanup(std::vector<cv::Mat> & io, const int & iter)
{
	for(int i=0; i<io.size(); i++)
	{
		cv::erode(io[i], io[i], cv::Mat(), cv::Point(-1, -1), iter);
		cv::dilate(io[i], io[i], cv::Mat(), cv::Point(-1, -1), iter);
	}
}

bool UTI::images::checkCoordinateValidity(cv::Point & tocheck, const cv::Size & inputSize)
{
	bool validity=true;

	if(tocheck.x<0)
	{
		tocheck.x=0;
		validity=false;
	}
	else if(tocheck.x>inputSize.width-1)
	{
		tocheck.x=inputSize.width-1;
		validity=false;
	}

	if(tocheck.y<0)
	{
		tocheck.y=0;
		validity=false;
	}
	else if(tocheck.y>inputSize.height-1)
	{
		tocheck.y=inputSize.height-1;
		validity=false;
	}

	return validity;
}

bool UTI::images::checkROIValidity(const cv::Size & imageSize, const cv::Rect & ROI)
{
	if(ROI.br().y>imageSize.height-1 || ROI.br().x>imageSize.width-1)
		return false;

	if(ROI.tl().x<0 || ROI.tl().y<0)
		return false;

	return true;
}

void UTI::images::createColoredMask(const cv::Mat & picture, const std::vector<cv::Mat> & partitions, const std::vector<int> & indexesToShow, cv::Mat & coloredMask)
{
	coloredMask=cv::Mat::zeros(picture.size(), CV_8UC3);
	for(int i=0; i<indexesToShow.size(); i++)
	{
		cv::Mat tmpMask;
		cv::bitwise_and(picture, picture, tmpMask, partitions[indexesToShow[i]]);
		cv::bitwise_or(tmpMask, coloredMask, coloredMask, cv::noArray());
	}
}

void UTI::images::drawPoints(const std::vector<cv::Point> & input, cv::Mat & output, const cv::Scalar & color)
{
	for(int i=0; i<input.size(); i++)
	{
		cv::circle(output, input[i], 5, color, -1);
	}
}

void UTI::images::drawLines(const std::vector<cv::Point> & polar_coordinates, cv::Mat & output, const cv::Scalar & color)
{
	float r, t;
	double a, b, x0, y0;
	cv::Point pt1, pt2;
	for(int i=0; i<polar_coordinates.size(); i++)
	{
		r=polar_coordinates[i].x;
		t=polar_coordinates[i].y;	
		a=cos(t);
		b=sin(t);
		x0=a*r;
		y0=b*r;
		pt1=cv::Point(cvRound(x0 + 1000*(-b)), cvRound(y0 + 1000*(a)));
		pt2=cv::Point(cvRound(x0 - 1000*(-b)), cvRound(y0 - 1000*(a)));
		cv::line(output, pt1, pt2, color, 4, 8);
	}
}

void UTI::images::drawRectangles(cv::Mat & io, const std::vector<cv::Rect> & inputs, const cv::Scalar & color, const bool differentColors)
{
	for(int i=0; i<inputs.size(); i++)
	{
		cv::Scalar tmpcolor;
		if(differentColors)
			tmpcolor=cv::Scalar(color[0]/(i+1), color[1]/(i+1), color[2]/(i+1));
		else
			tmpcolor=color;
		cv::rectangle(io, inputs[i], color, 1);
	}
}

void UTI::images::changeColorSpace(cv::Mat & io, const int & colorspace)
{
	cv::cvtColor(io, io, colorspace);
}

void UTI::images::blur(cv::Mat & io, const cv::Size & kernelSize, const std::string & type)
{
	if(!type.compare("blur"))	// normalized box filter
	{
		cv::blur(io, io, kernelSize);
	}
	else if(!type.compare("gblur"))	// gaussian filter
	{
		cv::GaussianBlur(io, io, kernelSize, 0, 0);
	}
	else if(!type.compare("mblur"))	// median filter
	{
		int ksize=0;		// median filter kernel must be odd
		if(kernelSize.width%2)
			ksize=kernelSize.width+1;
		else
			ksize=kernelSize.width;
		cv::medianBlur(io, io, ksize);
	}
	else if(!type.compare("bblur"))	// bilateral filter
	{
		cv::bilateralFilter(io, io, -1, kernelSize.width, kernelSize.height);
	}
}

void UTI::images::createColoredMask(const cv::Mat & colorpicture, const cv::Mat & bwmask, cv::Mat & colormask)
{
	cv::bitwise_and(colorpicture, colorpicture, colormask, bwmask);
}

void UTI::images::slidingMaxWindow(const cv::Mat & in, cv::Point & localMaxima, const int & windowSize)
{
  // pair<int, int> represents the pair (ARR[i], i)
  std::deque< std::pair<int, int> > window;
  int counter=0;
  for (int i = 0; i < in.size().height; i++) 
  {
	  //std::cout << "vert. " << i << "__";
	  for(int j=0; j<in.size().width; j++)
	  {
		  int value = in.at<ushort>(i, j);
		  //std::cout << "horz. " << i << "__";
		  counter=i*in.size().height+j;
		  //std::cout << "counter = " << counter << "__";
		  while (!window.empty() && window.back().first <= value)
			  window.pop_back();
		  //std::cout << "inserting " << value;
		  window.push_back(std::make_pair(value, counter));
		  //std::cout << " done\n";
		  while(window.front().second <= counter - windowSize)
			  window.pop_front();
	  }
  }
  
  int J=window.front().first%in.size().height;		// nb of rows
  int I=(window.front().first-J)/in.size().height;	// nb of columns
  //std::cout << "front: " << window.front().first << " J: " << J << " I: " << I << std::endl;
  localMaxima=cv::Point(J, I);
}

void UTI::images::computeHistogram(const cv::Mat & in, cv::Mat & hist)
{
	int histBins=256;		//0-255
	float range[]={0,256};
	const float * histRange={range};
	cv::calcHist(&in, 1, 0, cv::Mat(), hist, 1, &histBins, &histRange, true, false);
}

void UTI::images::plotHistogram(const cv::Mat & hist, cv::Mat & out, const cv::Scalar & color, const bool & initializeOutput)
{
	int hist_w = 512; int hist_h = 400;
    int bin_w = cvRound( (double) hist_w/256 );
	if(initializeOutput)
		out=cv::Mat(hist_h, hist_w, CV_8UC3, cv::Scalar(0,0,0));
	cv::Mat normHist;
	cv::normalize(hist.clone(), normHist, 0, out.rows, cv::NORM_MINMAX, -1, cv::Mat());
	for(int i=1; i<255; i++)
	{
		cv::line(out, cv::Point(bin_w*(i-1), hist_h - cvRound(normHist.at<float>(i-1))), cv::Point(bin_w*(i), hist_h - cvRound(normHist.at<float>(i))), color, 2, 8, 0);
	}
}

void UTI::images::computeHistogramMedian(const cv::Mat & hist, int & medianBin)
{
	cv::Scalar totalElements=cv::sum(hist);
	float target=totalElements[0]/2;
	float cumulativeElements=0;
	int bin=hist.rows-1;
	while(cumulativeElements<target && bin>=0)
		cumulativeElements+=hist.at<float>(bin--);
	medianBin=bin;
}

void UTI::images::findMaxLoc(const cv::Mat & in, cv::Point & maxLoc, double & maxVal)
{
	cv::minMaxLoc(in, NULL, &maxVal, NULL, &maxLoc);
}

void UTI::images::putText(cv::Mat & io, const cv::Point & location, const std::string & text)
{
	cv::putText(io, text, location, cv::FONT_HERSHEY_SIMPLEX, 1.0, cv::Scalar(0,0,255), 2);
}

void UTI::images::rotate(cv::Mat & io, const double & angle)
{
	cv::Point2f center=cv::Point(io.cols/2, io.rows/2);
	cv::Mat rotationMatrix=cv::getRotationMatrix2D(center, angle, 1.0);
	cv::Mat tmp;
	cv::warpAffine(io, tmp, rotationMatrix, io.size());
	io=tmp.clone();
}


//////////////////////////////
// MATH
////////////////////////////
double UTI::math::euclDist(const double & x1, const double & x2)
{
	double value=std::abs(x1-x2);
	return value;
}

double UTI::math::euclDist(const cv::Point3f & x1, const cv::Point3f & x2)
{
	double value=std::pow(x1.x-x2.x, 2);
	value+=std::pow(x1.y-x2.y, 2);
	value+=std::pow(x1.z-x2.z, 2);
	return std::sqrt(value);
}

double UTI::math::euclDist(const cv::Point2f & x1, const cv::Point2f & x2)
{
	double value=std::pow(x1.x-x2.x, 2);
	value+=std::pow(x1.y-x2.y, 2);
	return std::sqrt(value);
}

double UTI::math::getAngle(const cv::Point2f & x1, const cv::Point2f & x2)
{
	double theta=std::atan2(UTI::math::euclDist(x1.x, x2.x), UTI::math::euclDist(x1.y, x2.y));
	return theta;
}

double UTI::math::computeContourArea(const std::vector<cv::Point> & contour)
{
	double area;
	area=cv::contourArea(contour);
	return area;
}

double UTI::math::computeContourPerimeter(const std::vector<cv::Point> & contour)
{
	double P;
	P=cv::arcLength(contour, true);
	return P;
}

/////////////////////////////////////////////
// STRING & TEXT
////////////////////////////////////////////
std::string UTI::string::itostr(const int & in)
{
    //char buffer[255];
    //return(std::string(_itoa(in, buffer, 10)));
    return "";
}

//////////////////////////////////////////////////
// time measurement
/////////////////////////////////////////////////
void UTI::timer::start()
{
	started=clock();
}

void UTI::timer::showElapsed()
{
	finished=clock();
	std::cout << "# UTI timer : " << 1000*(float)(finished-started)/(float)CLOCKS_PER_SEC << " ms elapsed since last measurement\n";
	started=clock();
}

double UTI::timer::giveElapsed()
{
	finished=clock();
	double time=1000*(float)(finished-started)/(float)CLOCKS_PER_SEC;
	started=clock();
	return time;
}

void UTI::timer::showFPS(cv::Mat & io, const int & timeWindowSize)
{
	char buf[30];
	static std::vector<double> times=std::vector<double>(timeWindowSize, 0);
	static double meanTime=0;
	double currTime=giveElapsed();
	meanTime+=currTime/timeWindowSize;
	meanTime-=times[0]/timeWindowSize;
	times.push_back(currTime);
	times.erase(times.begin());
	int FPS=std::ceil(1000/meanTime);
//	std::string text=itoa(FPS, buf, 10);
//	text+=" FPS";
//	cv::putText(io, text, cv::Point(io.size().width/15, io.size().height/15), cv::FONT_HERSHEY_SIMPLEX, 1.0, cv::Scalar(0,0,255), 2);
}

float UTI::timer::stop()
{
	finished=clock();

	return 1000*(float)(finished-started)/(float)CLOCKS_PER_SEC;
}


//////////////////////////////////////////////////
// Camera undistorion
/////////////////////////////////////////////////
UTI::camera::camera()
{
	botHeight_cm=10;
}

void UTI::camera::initUndistorsion(const cv::Mat & cameraMatrix, const cv::Mat & DistorsionCoefficients, const cv::Size & size)
{
	cv::initUndistortRectifyMap(cameraMatrix, DistorsionCoefficients, cv::Mat(), cv::getOptimalNewCameraMatrix(cameraMatrix, DistorsionCoefficients, size, 1, size, 0), size, CV_16SC2,	map1, map2);
	K_matrix=cameraMatrix.clone();
	K_matrix_inv=K_matrix.inv();
	if(map1.empty())
		std::cout << "UTI::camera::initUndistorsion() warning : map1 is empty\n";
	if(map2.empty())
		std::cout << "UTI::camera::initUndistorsion() warning : map2 is empty\n";
	else
		std::cout << "UTI::camera::initUndistorsion() correctly initialized\n";
}

void UTI::camera::setRT_matrix(const cv::Point3i & T_cm, const double & pitchAngleDegrees, const double & yawAngleDegrees, const double & rollAngleDegrees)
{
	double sinP=sin(pitchAngleDegrees*CV_PI/180.0);
	double cosP=cos(pitchAngleDegrees*CV_PI/180.0);
	double sinY=sin(yawAngleDegrees*CV_PI/180.0);
	double cosY=cos(yawAngleDegrees*CV_PI/180.0);
	double sinR=sin(rollAngleDegrees*CV_PI/180.0);
	double cosR=cos(rollAngleDegrees*CV_PI/180.0);
	RT_matrix=cv::Mat::zeros(4, 4, CV_32FC1);
	cv::Mat Rz=cv::Mat::zeros(3, 3, CV_32FC1);		// yaw rotation matrix
	Rz.at<float>(0,0)=cosY;
	Rz.at<float>(0,1)=sinY;
	Rz.at<float>(1,0)=-sinY;
	Rz.at<float>(1,1)=cosY;
	Rz.at<float>(2,2)=1;
	cv::Mat Ry=cv::Mat::zeros(3, 3, CV_32FC1);		// pitch rotation matrix
	Ry.at<float>(0,0)=cosP;
	Ry.at<float>(0,2)=-sinP;
	Ry.at<float>(1,1)=1;
	Ry.at<float>(2,0)=sinP;
	Ry.at<float>(2,2)=cosP;
	cv::Mat Rx=cv::Mat::zeros(3, 3, CV_32FC1);		// roll rotation matrix
	Rx.at<float>(0,0)=1;
	Rx.at<float>(1,1)=cosR;
	Rx.at<float>(1,2)=sinR;
	Rx.at<float>(2,1)=-sinR;
	Rx.at<float>(2,2)=cosR;
	RT_matrix(cv::Rect(0,0,3,3))=Rz*Ry*Rx;		// global rotation matrix
	RT_matrix.at<float>(0,3)=T_cm.x;	// translation matrix
	RT_matrix.at<float>(1,3)=T_cm.y;
	RT_matrix.at<float>(2,3)=T_cm.z;
	RT_matrix.at<float>(3,3)=1;
	RT_matrix_inv=RT_matrix.inv();		// inverted matrix
}

void UTI::camera::performUndistorion(const cv::Mat & in, cv::Mat & out)
{
	cv::remap(in, out, map1, map2, cv::INTER_LINEAR);
}

void UTI::camera::transformCamCoordinatesToUAVCoordinates(const cv::Point & in, cv::Point & out, const int & PointZ)
{
	cv::Mat homogeneousIn=cv::Mat(4,1, CV_32FC1);
	cv::Mat homogeneousOut=cv::Mat(4,1, CV_32FC1);
	homogeneousIn.at<float>(0,0)=in.x;
	homogeneousIn.at<float>(1,0)=in.y;
	homogeneousIn.at<float>(2,0)=PointZ;
	homogeneousIn.at<float>(3,0)=1;
	homogeneousOut=RT_matrix_inv*K_matrix_inv*homogeneousIn;
	out.x=homogeneousOut.at<float>(0,0);
	out.y=homogeneousOut.at<float>(1,0);
}

void UTI::camera::transformCamCoordinatesToUAVCoordinates(const std::vector<cv::Point> & in, std::vector<cv::Point> & out, const int & pointsAltitude)
{
	cv::Point tmp;
	for(int i=0; i<in.size(); i++)
	{
		transformCamCoordinatesToUAVCoordinates(in[i], tmp, pointsAltitude);
		out.push_back(tmp);
	}
}

void UTI::camera::performPerspectiveCorrection(const cv::Mat & in, const std::vector<cv::Point> quadrilateralPoints, cv::Mat & out)
{
	// compute the points corresponding to the quadrilateral points in the DESTINATION image
	// should be the minimal bounding rectangle of the quadrilateral points in the SOURCE image
	cv::Mat tmpqp=cv::Mat(4,1,CV_32FC2), tmpqp2=cv::Mat(4,1,CV_32FC2);
	cv::RotatedRect dstQuadPts=cv::minAreaRect(quadrilateralPoints);
	cv::Point2f tmp[4];
	dstQuadPts.points(tmp);
	for(int i=0; i<4; i++)
	{
		tmpqp.ptr<float>(i)[2*0+0]=quadrilateralPoints[i].x;
		tmpqp.ptr<float>(i)[2*0+1]=quadrilateralPoints[i].y;
	}
	tmpqp2.ptr<float>(0)[2*0+0]=tmp[1].x;
	tmpqp2.ptr<float>(0)[2*0+1]=tmp[1].y;
	tmpqp2.ptr<float>(1)[2*0+0]=tmp[2].x;
	tmpqp2.ptr<float>(1)[2*0+1]=tmp[2].y;
	tmpqp2.ptr<float>(2)[2*0+0]=tmp[0].x;
	tmpqp2.ptr<float>(2)[2*0+1]=tmp[0].y;
	tmpqp2.ptr<float>(3)[2*0+0]=tmp[3].x;
	tmpqp2.ptr<float>(3)[2*0+1]=tmp[3].y;

	std::cout << "After persp.  correction : " << tmpqp2 << std::endl;
	std::cout << "Before persp. correction : " << tmpqp << std::endl;

	// get the perspective transformation matrix
	cv::Mat transMtx=cv::getPerspectiveTransform(tmpqp, tmpqp2);

	// Apply perspective transformation
	cv::warpPerspective(in, out, transMtx, out.size());
}

UTI::camera::~camera()
{

}


//////////////////////////////////////////////////
// FILE READING
/////////////////////////////////////////////////
bool UTI::fileReading::openFile(const std::string & name)
{
    /*
	file.open(name);
	if(file.fail())		// si d�faut d'ouverture
	{
		fprintf( stderr, "Warning: error opening %s, %s, line %d\n",
			name, __FILE__, __LINE__ );
		isOpened=false;
	}
	else
		isOpened=true;
	return isOpened;
    */
    return false;
}

bool UTI::fileReading::closeFile()
{
	file.close();
	if(file.is_open())
		isOpened=true;
	else
		isOpened=false;
	return !isOpened;
}

bool UTI::fileReading::readNextLine(std::string & content)
{
	static const int car_count=1000;		//assume 1000 characters max per line
	if(file.eof())
		return false;

	static char * delim=" ";
	if(!file.failbit)		// if too many characters on one line
	{
		std::cout << "more than " << UTI::string::itostr(car_count) << " characters in one line\n";
		return false;
	}

	file >> content;		// extract until a space

	return true;


}


//////////////////////////////
// VIDEO
////////////////////////////
UTI::video::video(const std::string & mode)
{
	readMode=false;
	writeMode=false;
	if(!mode.compare("read"))
		readMode=true;
	else if(!mode.compare("write"))
		writeMode=true;
}

bool UTI::video::create(const std::string & in)
{
	if(readMode)
	{
		videoInputObject.open(in);
		return videoInputObject.isOpened();
	}
	else if(writeMode)
	{
		std::cout << "Please specify the output video frame rate and size\n";
		return false;
	}
	else
		return false;
}

bool UTI::video::create(const std::string & in, const cv::Size & frameSize, const int & frameRate)
{
	if(readMode)
	{
		videoInputObject.open(in);
		return videoInputObject.isOpened();
	}
	else if(writeMode)
	{
		videoOutputObject.open(in, -1, frameRate, frameSize);
		return videoOutputObject.isOpened();
	}
	else
		return false;
}

cv::Size UTI::video::getFrameSize()
{
	if(readMode)
		return cv::Size((int)videoInputObject.get(CV_CAP_PROP_FRAME_WIDTH), (int)videoInputObject.get(CV_CAP_PROP_FRAME_HEIGHT));
	else
		return cv::Size();
}

bool UTI::video::nextFrame(cv::Mat & frame)
{
	bool success;
	if(readMode)
		success=videoInputObject.read(frame);
	else if(writeMode)
	{
		success=true;
		videoOutputObject.write(frame);
	}

	return success;
}

bool UTI::video::release()
{
	bool success;
	if(readMode)
	{
		videoInputObject.release();
		success=!videoInputObject.isOpened();
	}
	else if(writeMode)
	{
		videoOutputObject.release();
		success=!videoOutputObject.isOpened();
	}

	return success;
}

UTI::video::~video()
{
	UTI::video::release();
}



//////////////////////////////////////////////////
// 
/////////////////////////////////////////////////




//////////////////////////////////////////////////
// 
/////////////////////////////////////////////////
