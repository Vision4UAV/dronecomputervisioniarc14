#pragma warning(disable : 4006)
#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

#include "opencv2/opencv.hpp"

#include "Utilities/Utilities.h"
#include "Detectors/Detectors.h"
#include "IFF/IFF.h"
#include "Trackers/Trackers.h"
#include "RANSAC/RANSAC_lines.h"
#include "SegmentationModule/SegmentationModule.h"

//#define DEBUG_CV

class cameraUndistortion
{
private:
	UTI::camera cameraCorrectionEngine;
	cv::Mat cameraMatrix, distorsionCoefficients;
	cv::Size cameraPictureSize;
public:
	void initialize(const cv::Size & pictureSize, const cv::Mat & camera_matrix, const cv::Mat & distorsion_coefficients);
	bool run(const cv::Mat & in, cv::Mat & out);
};

class BotDetectionVJ
{
private:
	DET::cascadeDetector botDetector;
public:
	bool initialize(const std::string & modelPathAndName);
	bool initialize(const std::string & modelPathAndName, const cv::Size & minDetectionRectangleSize, const cv::Size & maxDetectionRectangleSize, const cv::Size & xThresholdsRASW, const cv::Size & yThresholdsRASW, const double & scaleFactor, const int & minNeighbors);
	bool run(const cv::Mat & in, std::vector<cv::Rect> & out);		
};

/*
class BotDetectionHOGSVM
{
private:
	DET::HOG_SVMDetector botDetector;
public:
	bool initialize(const std::string & modelPathAndName);
	bool run(const cv::Mat & in, std::vector<cv::Rect> & out);		
};
*/

class ColorSegmentationCSVM
{
private:
	SMOD::COLORSVM csvm;
	cv::Size wsize;
public:
	void initialize(const std::string & model_file, const std::string & scaleValues_file, const cv::Size & workingSize=cv::Size(140,105), const int & svm_type=cv::SVM::LINEAR, const int & colorspace=CV_BGR2YUV);
	bool run(const cv::Mat & in, cv::Mat & out);
};

class BotTrackingCamshift
{
private:
	std::vector<TRK::CamShift> botTracker;
	bool trackerInitialized;
	int successiveFramesTracked;
public:
	void initialize(const int & maxTrackers=5);
	bool computeReference(const cv::Mat & image, const cv::Mat & colorMask, const std::vector<cv::Rect> & in);		// run before starting the tracking
	bool track(const cv::Mat & image, const std::vector<cv::Rect> & in, std::vector<cv::Rect> & out, std::vector<double> & botOrientations);		
	void stopTracking();	// run when stopping the tracking
};

class BotIdentification
{
private:
	cv::Scalar wth;
	cv::Scalar rth;
	cv::Scalar gth;
	int minAngle_degrees;
	int maxAngle_degrees;
	double min_voteInputHeight_ratio;
	double verticalROI_extensionFactor;
	bool useColorClues;
	bool useHoughClues;
	void run_RGB_hough(const cv::Mat & image, const cv::Mat & mask, const std::vector<cv::Rect> & detector_output, std::vector<int> & labels);
	IFF IFFObj;
	// TODO : run_geometric();
public:
	void initialize(const cv::Scalar & whiteThresholds, const cv::Scalar & redThresholds, const cv::Scalar & greenThresholds,
		const double & HoughMin_voteInputHeight_ratio=0.50, const int & HoughMinAngle_degrees=70, 
		const int & HoughMaxAngle_degrees=110, const double & HoughVerticalROI_extensionFactor=4, bool useColor=true, bool useHough=true);
	void run(const cv::Mat & image, const cv::Mat & mask, const std::vector<cv::Rect> & detector_output, std::vector<int> & labels);	// -1 pole (color+geometric clue), 0 unknown (color+geo), 1 green (color+geo), 2 red (color+geo), 3 white
};

class BotBaptizer
{
private:
	std::vector<cv::Rect> previousPositions;
	std::vector<int> previousLabels;
	std::vector<int> previousTypes;
	int consecutiveNotTreatedFrames;
	int max_consecutiveNotTreatedFrames;
	int maxAllowedDrift;
	int currentID_counter;
	bool useColorMatching;
	bool checkPositionDrift(const cv::Rect & oldPosition, const cv::Rect & newPosition);
public:
	BotBaptizer();
	void init(const int & maxAllowedPositionDrift_pixels=80, const bool & useTypeMatching=false, const int & maxSkippedFrames=10);
	void run(const std::vector<cv::Rect> & currentPositions, const std::vector<int> & currentTypes, std::vector<int> & currentLabels);
};

class GridLineFinder	
{
private:
    double y_channel_thresh;
private:	
	// intersection functions
	bool autothreshold(const cv::Mat & in, const cv::Size & workSize, cv::Mat & out, const int & erosionIter=2);		// perform adaptive threshold, 2 erosions by default
	bool floodFillGridMask(const cv::Mat & picture, const cv::Mat &autothresholdSeg, cv::Mat & out);
	bool canny(const cv::Mat & in, cv::Mat & out);
	bool refineAndValidateColorMask(cv::Mat & io);
	// shared functions
	bool DTSampling(const cv::Mat & BWmask, std::vector<cv::Point> & localMaxima, const cv::Size & windowCount);
	bool useRANSAC2pass(const cv::Mat & in, std::vector<cv::Point> & out, const bool isKeyline);
	cv::Size wsize;		// working size to which pictures will be reduced for speeding up segmentation purposes
	cv::Size DT_cellCount;		// for distance transform-based local maxima sampling
	// intersections parameters
	cv::Vec3i BGR_refinementThresholds;			// for validating or not an intersection color mask
	double refinement_whitePixelOccupancyRatio;	// for validating or not an intersection color mask
	// RANSAC parameters
	CASNAR ransacobj;		// ransac object
	int RANSAC_ITER;
	int RANSAC_MAX_INLIER_DISTANCE;
	int RANSAC_MIN_INLIER_COUNT;
public:
	// inits
	GridLineFinder(const cv::Size workingWindowSize=cv::Size(140,105), 
		const cv::Vec3i BGR_refinementThresholdsForBG=cv::Vec3i(80, 100, 150), const double maskMaxWhiteRatio=0.15, 
		const cv::Size DT_cellCountXY=cv::Size(30, 20), const int RANSCAC_MAXIMUM_ITERATIONS=1000, 
		const int RANSAC_MAXIMUM_INLIER_DISTANCE=4, const int RANSAC_MINIMUM_INLIER_COUNT=13, const int Y_channel_threshold=248);
	
	// use
	bool run(const cv::Mat & undistortedFrame, std::vector<cv::Point> & out);
	void intersectionClustering(const std::vector<cv::Point> & in, std::vector<cv::Point> & out, const int & clusteringRadius=50);	
};

class GridIntersectionTracker
{
private:
	std::vector<TRK::Kalman> gridTracker;
public:
	void initialize(const int & maxTrackers, const double & Vx_weight=1.0, const double & Vy_weight=1.0, const double & Ax_weight=0.5, const double & Ay_weight=0.5);
	bool track(const std::vector<cv::Point> & gridIntersections, std::vector<cv::Point> & predictedIntersections);
	bool updateTracker(const std::vector<cv::Point> & gridIntersections, std::vector<cv::Point> & estimatedIntersections);
};
