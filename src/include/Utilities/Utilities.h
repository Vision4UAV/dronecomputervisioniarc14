#pragma warning(disable : 4006)
#pragma once 

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <time.h>

#include "opencv2/opencv.hpp"


namespace UTI
{
	class exports
	{
	private:

	public:
		static void savePicture(const cv::Mat & picture, const std::string repName, const std::string & name, const std::string & ext, const int & counter);
	};

	class images
	{
	private:

	public:
		static void resize(cv::Mat & io, const cv::Size & processingSize);
		static void separatePartitions(const cv::Mat & in, const int & num, std::vector<cv::Mat> & out);
		static void morphologyCleanup(std::vector<cv::Mat> & io, const int & iter);
		static bool checkCoordinateValidity(cv::Point & tocheck, const cv::Size & inputSize);
		static bool checkROIValidity(const cv::Size & imageSize, const cv::Rect & ROI);
		static void createColoredMask(const cv::Mat & picture, const std::vector<cv::Mat> & partitions, const std::vector<int> & indexesToShow, cv::Mat & coloredMask);
		static void drawPoints(const std::vector<cv::Point> & input, cv::Mat & output, const cv::Scalar & color);
		static void drawLines(const std::vector<cv::Point> & polar_coordinates, cv::Mat & output, const cv::Scalar & color);
		static void drawRectangles(cv::Mat & io, const std::vector<cv::Rect> & inputs, const cv::Scalar & color, const bool differentColors);
		static void changeColorSpace(cv::Mat & io, const int & colorspace);
		static void createColoredMask(const cv::Mat & colorpicture, const cv::Mat & bwmask, cv::Mat & colormask);
		// blurring. type is : blur | gblur | mblur | bblur
		static void blur(cv::Mat & io, const cv::Size & kernelSize, const std::string & type);
		static void slidingMaxWindow(const cv::Mat & in, cv::Point & localMaxima, const int & windowSize);
		static void computeHistogram(const cv::Mat & in, cv::Mat & hist);
		static void plotHistogram(const cv::Mat & hist, cv::Mat & out, const cv::Scalar & color, const bool & initializeOutput);
		static void computeHistogramMedian(const cv::Mat & hist, int & medianBin);
		static void findMaxLoc(const cv::Mat & in, cv::Point & maxLoc, double & maxVal);
		static void putText(cv::Mat & io, const cv::Point & location, const std::string & text);
		static void rotate(cv::Mat & io, const double & angle);
	};

	class math
	{
	private:

	public:
		static double euclDist(const double & x1, const double & x2);	
		static double euclDist(const cv::Point2f & x1, const cv::Point2f & x2);	
		static double euclDist(const cv::Point3f & x1, const cv::Point3f & x2);	
		static double getAngle(const cv::Point2f & x1, const cv::Point2f & x2);
		static double computeContourArea(const std::vector<cv::Point> & contour);
		static double computeContourPerimeter(const std::vector<cv::Point> & contour);
	};

	class string
	{
	private:

	public:
		static std::string itostr(const int & in);
	};

	class vector
	{
	private:

	public:
		static void sort();	//todo
	};

	class timer
	{
	private:
		time_t started;
		time_t finished;
	public:
		void start();
		void showElapsed();
		double giveElapsed();
		void showFPS(cv::Mat & io, const int & timeWindowSize);
		float stop();	// returns the ellapsed time in ms
	};

	class camera
	{
	private:
		cv::Mat map1;
		cv::Mat map2;
		cv::Mat K_matrix;
		cv::Mat K_matrix_inv;
		cv::Mat RT_matrix;
		cv::Mat RT_matrix_inv;
		int botHeight_cm;
	public:
		camera();
		void initUndistorsion(const cv::Mat & cameraMatrix, const cv::Mat & DistorsionCoefficients, const cv::Size & size);
		void setRT_matrix(const cv::Point3i & T, const double & pitchAngleDegrees, const double & yawAngleDegrees, const double & rollAngleDegrees);
		void performUndistorion(const cv::Mat & in, cv::Mat & out);
		void transformCamCoordinatesToUAVCoordinates(const cv::Point & in, cv::Point & out, const int & PointZ);
		void transformCamCoordinatesToUAVCoordinates(const std::vector<cv::Point> & in, std::vector<cv::Point> & out, const int & pointsAltitude);
		// quadrilateralPoints contains the points (in the SOURCE image) that should form a quadrilateral shape once 
		// the perspective is corrected. Fill it starting from the top-left point and continue clockwise.
		void performPerspectiveCorrection(const cv::Mat & in, const std::vector<cv::Point> quadrilateralPoints, cv::Mat & out);
		~camera();
	};

	class fileReading
	{
	private:
		std::string filename;
		std::ifstream file;
		bool isOpened;
	public:
		bool openFile(const std::string & name);	// returns isOpened
		bool closeFile();							// returns !isOpened
		bool readNextLine(std::string & content);	// returns false if file.eof()
	};

	class video
	{
	private:
		cv::VideoWriter videoOutputObject;
		cv::VideoCapture videoInputObject;
		bool writeMode;
		bool readMode;
	public:
		video(const std::string & mode);
		bool create(const std::string & in);
		bool create(const std::string & in, const cv::Size & frameSize, const int & frameRate);
		cv::Size getFrameSize();
		bool nextFrame(cv::Mat & frame);
		bool release();
		~video();
	};

}
