#pragma warning(disable : 4006)
#pragma once

#include <stdio.h>
#include <stdlib.h>


#include "opencv2/opencv.hpp"

#include "Utilities/Utilities.h"
//#include "SvmLightLib.h"		// Use SVMLightClassifier library for everything related to the SVM training
								// https://github.com/LihO/SVMLightClassifier
#include "RASW.h"

namespace DET
{
	class cascadeDetector		// attempting at speeding it up with RASW
	{
	private:
		cv::Size minSize, maxSize;
		bool findBiggest;
		cv::CascadeClassifier VJmodel;
		RASW_extended_VJ VJmodel_RASW;
		double scaleFactor;
		int minNeighbors;
		bool manualInit;
		bool modelIsLoaded;
		bool usesRASW;
		cv::Point xThreshs, yThreshs;
	public:
		cascadeDetector();
		cascadeDetector(const cv::Size & minDetectionSize, const cv::Size & maxDetectionSize, const bool & onlyOneObject, const double & iterationScaleFactor, const int & minimumNeighbors);
		cascadeDetector(const cv::Size & minDetectionSize, const cv::Size & maxDetectionSize, const bool & onlyOneObject, const double & iterationScaleFactor, const int & minimumNeighbors, const cv::Point & xThresholds, const cv::Point & yThresholds);
		bool loadModel(const std::string & model);
		bool detect(const cv::Mat & in, std::vector<cv::Rect> & ROIs);
		bool detectAndDisplay(const cv::Mat & in, std::vector<cv::Rect> & ROIs, cv::Mat & out);
		~cascadeDetector();
	};

/*
	class HOG_SVMDetector
	{
	private:
		cv::HOGDescriptor HOGmodel;
		double hitThresh;
		cv::Size windowStride;
		cv::Size winSize;
	public:
		HOG_SVMDetector();
		HOG_SVMDetector(const double & hitThreshold, const cv::Size & WinSize, const cv::Size & WinStride, const cv::Size & BlockSize, const cv::Size & CellSize, const int & binAmount, const double & clippingValue, const int & NLevels, const bool & useGammaPreProcessing);
		void trainSVM(const std::string & inputIndexFile, const std::string & outputVectorFile);
		bool loadModel(const std::string & inputVectorFile);
		bool detect(const cv::Mat & in, std::vector<cv::Rect> & ROIs);
		bool detectAndDisplay(const cv::Mat & in, std::vector<cv::Rect> & ROIs, cv::Mat & out);
		~HOG_SVMDetector();
	};
    */
}
