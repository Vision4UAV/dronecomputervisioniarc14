#pragma warning(disable : 4006)
#pragma once
#include <stdio.h>
#include <iostream>
#include <string>

#include "opencv2/opencv.hpp"

#include "Utilities/Utilities.h"
//#include "SegmentationModule.h"
#include "CSAMPEDRO_HOUGH/Hough.h"



class IFF
{
private:
	// HOUGH
	int minAngle_degrees;
	int maxAngle_degrees;
	double min_voteInputHeight_ratio;
	double verticalROI_extensionFactor;
	CSH::Hough HoughObj;

	// color
	cv::Scalar greenThresholds;
	cv::Scalar redThresholds;
	cv::Scalar whiteThresholds;

	void segmentBot(const cv::Mat & input, cv::Mat & output);
	int useShapeInfo(const cv::Mat & input, const double & minCircularity, const double & minEccentricity);	// return if it is a bot (+1) or not (-1) or unknown (0).
	// void useColorInfoHSV(const cv::Mat & input, const cv::Scalar & minGreen, const cv::Scalar & maxGreen, const cv::Scalar & minRed, const cv::Scalar & maxRed, const cv::Scalar & minWhite, const cv::Scalar & maxWhite, std::string & botColor);	// Give the ratios for each histogram
	cv::Scalar useColorInfoRGB(const cv::Mat & input, std::string & botColor);	// Give the ratios for each histogram
	int checkPole(const cv::Mat & input);
	void mergeDecisions(const std::string & botColor_color, const int & isPole, std::string & botColor);
public:
	void initialize(const cv::Scalar & botWhiteThresholds, const cv::Scalar & botRedThresholds, 
		const cv::Scalar & botGreenThresholds, const double & HoughMin_voteInputHeight_ratio=0.50, const int & HoughMinAngle_degrees=70, 
		const int & HoughMaxAngle_degrees=110, const double & HoughVerticalROI_extensionFactor=4);
	void run(const cv::Mat & segmentationROI, const cv::Mat & input, const cv::Rect & ROI, std::string & botColor, const bool useColor=true, 
		const bool useHough=true);
};
