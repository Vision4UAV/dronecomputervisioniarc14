#pragma warning(disable : 4006)
#pragma once

#include <stdio.h>
#include <stdlib.h>

#include "opencv2/opencv.hpp"
#include "Utilities/Utilities.h"


namespace TRK
{
	// Kalman filter - 2D tracking
	class Kalman
	{
	private:
		cv::KalmanFilter KF;
		cv::Mat transitionMatrix;
		bool first;
		void firstUse(const cv::Point & state_start);
	public:
		Kalman();
		void setTransitionMatrixWeights(const double & Vx_weight, const double & Vy_weight, const double & Ax_weight, const double & Ay_weight);
		void init(const int & stateDims, const int & measurementDims, const int & controlDims);
		void predict(const cv::Point & first_state, cv::Point & state_predicted);
		void correct(const cv::Point & stateMeasurement, cv::Point & state_k);
		~Kalman();
	};

	class CamShift
	{
	private:
		void computeHist(const cv::Mat & trackerInitImage, const cv::Mat & mask);
		void computeBackProj(const cv::Mat & image);
		cv::MatND backproj;
		cv::MatND hist;
		double objectOrientation;
	public:
		bool initializeTracking(const cv::Mat & trackerInitImage, const cv::Mat & mask=cv::Mat());
		bool performTracking(const cv::Mat & image, cv::Rect & searchWindowInit, cv::RotatedRect & objectLocation, double & objectOrientation);
	};

}
