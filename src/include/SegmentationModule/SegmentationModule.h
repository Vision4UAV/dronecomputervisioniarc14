#pragma warning(disable : 4006)
#pragma once

#include <stdio.h>
#include <stdlib.h>

#include <fstream>

#include "opencv2/opencv.hpp"

#include "Utilities/Utilities.h"
//#include "RegionGrowing.hpp"

//XML parser
#include "pugixml.hpp"




namespace SMOD
{
	/////////////////////////////////
	// KMEANS
	////////////////////////////////
	class KM
	{
	private:
		
	public:
		static void run(const cv::Mat & in, cv::Mat & out, const int & K, const cv::Size & processingSize);

	};

	/////////////////////////////////
	// MEANSHIFT
	////////////////////////////////
	class MS
	{
	private:

	public:
		static void run(const cv::Mat & in, cv::Mat & Out, const int & spatialRes, const int & colorRes, const int & maxLevel, const cv::Size & processingSize); 
	};

	/////////////////////////////////
	// THRESHOLDING
	////////////////////////////////
	class TH
	{
	private:

	public:
		static void run_std(const cv::Mat & in, cv::Mat & out, const cv::Vec3i thresholds, const cv::Size & processingSize);
		static void run_std(const cv::Mat & in, cv::Mat & out, const cv::Vec3i thresholds, const cv::Size & processingSize, const int threshType);
		static void run_auto(const cv::Mat & in, cv::Mat & out_mean, const int & kernelSize, const cv::Size & processingSize);
		static void run_autoG(const cv::Mat & in, cv::Mat & out_gaussian, const int & kernelSize, const cv::Size & processingSize);
	};

	/////////////////////////////////
	// EDGE DETECTORS
	////////////////////////////////
	class EDGE
	{
	private:

	public:
		// theoretical lack of accuracy for the following algorithms
		static void canny(const cv::Mat & input, cv::Mat & edges, cv::Point Thresholds);	// 
		static void sobel();	// todo. inferior to canny according to litterature
		static void scharr();	// todo. inferior to canny according to litterature
		static void prewitt();	// todo
		// todo precornersdetect ?
		// todo goodFeaturesToTrack ?
		// theoretical sensitivity to noise for the following algorithms
		static void laplacian();	// todo. inferior to canny according to litterature
		// other algorithms are time consuming
	};

	/////////////////////////////////
	// SVM COLOR SEGMENTATION
	////////////////////////////////
	class COLORSVM
	{
	private:
		cv::SVM svmObject;
		cv::SVMParams svmParameters;
		cv::Mat DataRowSamples;
		cv::Mat LabelsRowSamples;
		bool colorSpaceSet;
		bool parametersSet;
		bool DataSet;
		bool scaleValuesComputed;
		bool SVMTrained;
		int usedColorspace;
		int pictureChannels;
		std::vector<cv::Point2d> scaleValuesPerChannel;
		void transformPictureToData(const cv::Mat & pictureF, const int & label);
		void scaleData();
		void transformDataToMask(const cv::Mat & labelMatrix, const cv::Size & maskSize, cv::Mat & mask);
		void saveScaleValues(const std::string & scaleValuesPathAndName);
		void loadScaleValues(const std::string & scaleValuesPathAndName);
        bool loadScaleValuesXML(const std::string & scaleValuesPathAndName);
		void removeValueChannel(cv::Mat & io);
	public:
		COLORSVM();
		void manualConstructor();
		void setSVMParameters(const int & kernelType);							// this must be called once 
		void setColorspace(const int & colorspace);								// this must be called once 
		void addToTrainingData(const cv::Mat & data, const int & label);		// labels must be >= 0
		bool trainSVM_withoutCrossValidation();
		bool trainSVM_withCrossValidation();
		bool saveModel(const std::string & modelPathAndName, const std::string & scaleValuesPathAndName);
		void loadModel(const std::string & modelPathAndName, const std::string & scaleValuesPathAndName);
		void segmentPicture(const cv::Mat & in, cv::Mat & out);
	};

	/////////////////////////////////
	// HISTOGRAMS (also use in desc)
	////////////////////////////////


	/////////////////////////////////
	// TEXTURE (also use in desc)
	////////////////////////////////

	/////////////////////////////////
	// MOMENTS (? seg ?)
	////////////////////////////////

	/////////////////////////////////
	// GRABCUT
	////////////////////////////////
	class grabcut
	{
	private:

	public:
		static void run(const cv::Mat & in, const cv::Mat & fgMask, const cv::Mat & probfgMask, const cv::Mat & probbgMask, const cv::Mat & bgMask, cv::Mat & out);
	};

	/////////////////////////////////
	// WATERSHED
	////////////////////////////////
	class watershed
	{
	private:

	public:
		static void run(const cv::Mat & in, const std::vector<cv::Mat> & seedMasks, cv::Mat & out);
	};

	/////////////////////////////////
	// REGION GROWING
	////////////////////////////////

	/////////////////////////////////
	// ACTIVE CONTOURS
	////////////////////////////////

	/////////////////////////////////
	// (in desc) SHAPE, TEMPLATE MATCHING (after straightening of the insulator's axis)
	////////////////////////////////

	/////////////////////////////////
	// (in desc) geometric quantities (compacity etc) -> see Plant leaf segmentation - shape description.pdf
	// (in desc) fourier descriptiors
	////////////////////////////////
}
