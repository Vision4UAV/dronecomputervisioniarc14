#pragma warning(disable : 4006)
#pragma once 


#include <stdio.h>
#include <iostream>
#include <vector>

#include "opencv2/opencv.hpp"


struct line_model
{
	// Ax+By=C=0
	bool vertical;
	bool horizontal;
	float A;
	float C;
	cv::Point pt1, pt2;
	std::vector<cv::Point> inliers;
	int votes;
};

// utility dependant on the structure line_model
bool computeLineIntersections(const std::vector<line_model> & lines, std::vector<cv::Point> & intersections, std::vector<float> & angle_delta);

class CASNAR	// RANSAC name conflicts with opencv...
{
private:
	cv::RNG rng;
	struct line_model model;
	std::vector<cv::Point> data;
	int Ti;	// distance threshold for inliers
	int Tv;	// threshold for number of inliers (votes) required for considering the model as valid
	int compute_inliers();
	struct line_model fit_line();
	void sortModelsByDescendingVotes(const std::vector<line_model> & models, std::vector<int> & index);
public:
	CASNAR();
	line_model compute_model(const cv::Point & pt1, const cv::Point & pt2);
	void lines(const std::vector<cv::Point> & in, std::vector<line_model> & models, const int & iterations, const int & maxdist, const int & minvotes);
	void secondPass(const std::vector<line_model> & models, std::vector<line_model> & important_models);
	~CASNAR();
};
