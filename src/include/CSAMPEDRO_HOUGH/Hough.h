#pragma once
#include <opencv2/opencv.hpp>
using namespace cv;

namespace CSH
{
	class Hough
	{
	private:
		vector<Vec2f> lines_hough;
		Mat Espacio_Hough;
		int rho_min,rho_max;
		int theta_min,theta_max;
        std::vector< std::vector<double> > trigoLookupTable;	// [0]=cos, [1]=sin
		bool isDebug;		// are we showing debug info (true) or just using it ?
		void createLookupTable();
	public:
		Hough();
		Hough(const bool & DBGvalue);	// are we showing debug info (true) or just using it ?
		~Hough(void);
		Mat EspacioHough(const cv::Mat &I_bordes, const double & theta_min, const double & theta_max, const double & theta_increment);
		Mat EspacioHough_OPT(const cv::Mat &I_bordes, const double & theta_min, const double & theta_max, const double & theta_increment);		// Optimized version
		std::vector<cv::Point> MaximosHough(const Mat &E_Hough,const int & num_picos,const int & vote_threshold,const int neighborhood_size);
		std::vector<cv::Point> MaximosHough(const Mat &E_Hough,const int & num_picos,const int & vote_threshold,const int neighborhood_size, std::vector<int> & votes);	// to know the vote amount for each line
		Mat LineasHough(Mat &I, const std::vector<cv::Point> & rho_theta);
	};

	class OCV_Hough
	{
	private:
        std::vector< std::vector<double> > trigoLookupTable;	// [0]=cos, [1]=sin
		std::vector<cv::Point> icvHoughLinesStandard( const cv::Mat & input, const double & rho_increment, const int & theta_min, const int & theta_max, const int & theta_increment, const int & num_peaks, const int & vote_threshold, const int neighborhood_size);
	public:
		OCV_Hough();
		std::vector<cv::Point> getLines(const cv::Mat & InputArray, const double & rho_increment, const double & theta_min, const double & theta_max, const double & theta_increment, const int & num_peaks, const int & vote_threshold, const int neighborhood_size);
		~OCV_Hough();
	};
}

