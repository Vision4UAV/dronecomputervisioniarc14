#pragma warning(disable : 4006)
#pragma once

#include "IARC_2014_wrapper/Full_IARC_CV.h"

class IARC14_grid_node
{
private:
	//////////////////////////
	// objects
	cameraUndistortion camObj;
	GridLineFinder gridObj;

	//////////////////////////
	// variables
	// frames
	cv::Mat orgFrame;
	cv::Mat undistortedFrame;

	// grid
	int intersectionClusteringRadius;

	///////////////////////
	// functions
	// for undistorting picture.
	bool undistortFrame(const cv::Mat & frame, cv::Mat & out);

public:
	// for setting up the camera parameters. Needs to be initialized for the rest to work properly.
	void initializeCameraParameters(const cv::Size & frameSize, const cv::Mat & cameraMatrix, 
		const cv::Mat & distorsionCoefficients);

    void initializeCameraParameters(std::string configFile);

	// for setting up the grid intersection detection
	// see parameter explanation in IARC_interface_reduced_test.cpp
	void initializeGridModule(const cv::Size workingWindowSize=cv::Size(140,105), const cv::Vec3i BGR_refinementThresholdsForBG=cv::Vec3i(80, 100, 150), 
		const double maskMaxWhiteRatio=0.15, const cv::Size DT_cellCountXY=cv::Size(30, 20), const int RANSCAC_MAXIMUM_ITERATIONS=10000, 
		const int RANSAC_MAXIMUM_INLIER_DISTANCE=5, const int RANSAC_MINIMUM_INLIER_COUNT=19, 
		const int & IntersectionClusteringRadius=60, const int & Y_channel_treshold=248);

	// using the node
	// output error codes are :
	// -1 failed to undistort
	// -2 failed to find intersections
	int run(const cv::Mat & frame, cv::Mat & undistortedFrameOutput, std::vector<cv::Point> & intersections);

    // debugging/visualization
    void showResults(cv::Mat & picture, const std::vector<cv::Point> & intersections);
};

class IARC14_bot_node
{
private:
	//////////////////////////
	// objects
	cameraUndistortion camObj;
	ColorSegmentationCSVM csvmObj;
	BotDetectionVJ vjObj;
	BotTrackingCamshift trackingObj;
	BotIdentification IFFObj;
	BotBaptizer labelObj;

	//////////////////////////
	// variables
	// frames
	cv::Mat orgFrame;
	cv::Mat undistortedFrame;
	cv::Mat csvmMask;

	// bots
	std::vector<cv::Rect> botROIs;
	std::vector<double> botOrientations;		// useless for now, not working due to uav jolts

	// tracking
	int FramesToDetect;		// not used, leave at one
	int FramesToTrack;
	int numBotsToTrack;

	///////////////////////
	// functions
	// for undistorting picture.
	bool undistortFrame(const cv::Mat & frame, cv::Mat & out);
	// for providing color clues. 
	bool segmentFrame(const cv::Mat & in, cv::Mat & out);

public:
	// for setting up the camera parameters. Needs to be initialized for the rest to work properly.
	void initializeCameraParameters(const cv::Size & frameSize, const cv::Mat & cameraMatrix, 
		const cv::Mat & distorsionCoefficients);
    void initializeCameraParameters(std::string configFile);

	// for setting up color segmentation by an SVM. Needs to be initialized for bot identification to work.
	void initializeColorSVM_segmentation(const std::string & csvm_model, const std::string & csvm_scaleValues, 
		const cv::Size & workSize=cv::Size(140,105), const int & svm_type=cv::SVM::LINEAR, const int & colorspace=CV_BGR2Lab);

	// for setting up bot detection, tracking  and identification. 
	// see parameter explanation in IARC_interface_reduced_test.cpp
	// as you will probably need to set manually the color thresholds to adapt them to your environment
	void initializeDetectionModule(const std::string & modelPathAndName, 
		const cv::Size & minDetectionRectangleSize=cv::Size(50,50), const cv::Size & maxDetectionRectangleSize=cv::Size(200, 200), 
		const cv::Size & xThresholdsRASW=cv::Point(10,13), const cv::Size & yThresholdsRASW=cv::Point(10,13), 
		const double & scaleFactor=1.10, const int & minNeighbors=3, const int & successiveFramesToDetect=1, 
		const int & successiveFramesToTrack=3, const int & maxBotsToTrack=4);

	void initializeIdentificationModule(const cv::Scalar & whiteThresholds=cv::Scalar(150,150,150), 
		const cv::Scalar & greenThresholds=cv::Scalar(30, 30, 10), const cv::Scalar & redThresholds=cv::Scalar(60,20,20), 
		const double & HoughMin_voteInputHeight_ratio=0.50, const int & HoughMinAngle_degrees=70, 
		const int & HoughMaxAngle_degrees=110, const double & HoughVerticalROI_extensionFactor=4, bool useColor=true, 
		bool useHough=true);
		
	// for setting up bot labelling
	// you can use (or not) type matching in addition to proximity-based matching. Preferably not.
	void initializeLabellingModule(const int & maxAllowedPositionDrift, const bool & useTypeMatching, const int & maxSkippedFrames);


	// using the node
	// output error codes are :
	// -1 failed to undistort
	// -2 failed to segment
	// -3 failed to detect/track
	// -4 failed to perform identification
	//  0 everything OK
    int run(const cv::Mat & frame, cv::Mat & undistortedFrameOutput, std::vector<cv::Rect> & botBoundingBoxes, std::vector<int> & botType, std::vector<int> & botLabels);

    // debugging/visualization
    void showResults(cv::Mat & picture, const std::vector<cv::Rect> & botBoundingBoxes, const std::vector<int> & botLabels);
};
